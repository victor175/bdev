# coding: utf-8

from django.db import models
from django.contrib.auth.models import User
from photologue.models import Gallery, Photo

EVENT_TITLE_MAX_LENGTH = 50
USER_ABOUT_MAX_LENGTH = 300

class CustomFields(models.Model):
	user = models.OneToOneField(User, related_name='custom') 
	about = models.CharField(max_length=USER_ABOUT_MAX_LENGTH, null=True, blank=True)
	photo = models.OneToOneField(Photo, related_name="photo", null=True)
	galleries = models.ManyToManyField(Gallery, related_name='galleries', null=True)

class Event(models.Model):
	title = models.CharField(max_length=EVENT_TITLE_MAX_LENGTH, unique=True)
	description = models.CharField(max_length=150, default='')
	start_date = models.DateTimeField()
	end_date = models.DateTimeField(null=True)
	cover_photo = models.ForeignKey(Photo, related_name='cover_photo', null=True)
	gallery = models.OneToOneField(Gallery, related_name='gallery', null=True)
	organisator = models.ForeignKey(User, related_name='organisator')
	observers = models.ManyToManyField(User, related_name='observers', null=True)
	moderators = models.ManyToManyField(User, related_name='moderators', null=True)

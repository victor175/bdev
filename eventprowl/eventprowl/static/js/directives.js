function datetimeHolderDirective() {       
	return {
		link: function(scope, element, attrs) {
			element = convertElemToJq(element);

			var timestamp = attrs.datetime,
				datetime = createCustomDateTimeObject(timestamp),
				dayHolder = $(element.find('.day')[0]),
				monthHolder = $(element.find('.month')[0]),
				yearHolder = $(element.find('.year')[0]),
				timeHolder = $(element.find('.time')[0]);

			dayHolder.text(datetime.day);
			monthHolder.text(datetime.month);
			yearHolder.text(datetime.year);
			timeHolder.text(datetime.time)
		}
	}
}

function eventRemovalButtonDirective() {       
	return {
		link: function(scope, element, attrs) {
			element = convertElemToJq(element);

			if(!document.location.hash.contains('profile')) {
				element.remove();
			} else {
				element.click(function () {
					var eventTitle = element.attr('data-title'),
						removeEventModal = $($('.remove-event-modal')[0]),
						modalTitle = removeEventModal.find('.modal-title'),
						removalForm = removeEventModal.find('#event-removal-form'),
						$rootScope = getRootScope(),
						eventRemovalUrl = $rootScope.getEventRemovalUrl(eventTitle),
						initialTitle = modalTitle.text();

					modalTitle.text(modalTitle.text() + '"' + eventTitle + '"');
					removalForm.attr('action', eventRemovalUrl);
					removeEventModal.modal('show');

					onModalStateChange(removeEventModal, function (isVisible, changeListenerInterval) {
						if(!isVisible) {
							modalTitle.text(initialTitle);
							clearInterval(changeListenerInterval);
						}
					});

					removalForm.submit(function() {
						commonFormRequest(this)
							.then(function() {
								closeModal(removeEventModal);
							})
							.then(function () {
								if(!removeEventModal.data('bs.modal').isShown) {
									removeEventFromLoaded(eventTitle);
									modalTitle.text(initialTitle);
								}
							})
							.fail(function (error) {
								console.log(error);
							});

						return false;
					});
				});
			}
		}
	}
}

function eventOrganisatorButtonDirective() {       
	return {
		link: function(scope, element, attrs) {
			element = convertElemToJq(element);

			if(document.location.hash.contains('profile/organisator')) {
				element.remove();
			}
		}
	}
}

function observeEventButtonDirective() {
	return {
		link: function(scope, element, attrs) {
			element = convertElemToJq(element);
			var $rootScope = getRootScope(),
				userType = $rootScope.userType,
				userTypes = $rootScope.userTypes;

			if(!document.location.hash.contains('profile/observer') && userType == userTypes.observer) {
				element.click(function () {
					var eventTitle = element.attr('data-title'),
						eventObservationSubmissionUrl = $rootScope.eventObservationSubmissionUrl,
						method = 'POST',
						data = {
							observation_event_title: eventTitle,
						};

					makeRequestWithAjax(eventObservationSubmissionUrl, method, data)
						.then(function (data) {
			  				if(data == '') {
								incrementEventObservers(eventTitle);
							}
						})
						.fail(function (error) {
							console.log(error);
						});
				});
			}
		}
	}
}

function shortEventHolderDirective() {
	return {
		link: function(scope, element, attrs) {
			element = convertElemToJq(element);

			initShortEventHolder(element, scope);
		}
	}
}

function gallerySelectDirective() {
	return {
		link: function(scope, element, attrs) {
			element = convertElemToJq(element);

			var isEditable = document.location.hash.contains('profile/organisator'),
				coverPhotoSelect = $('#select-cover-photo'),
				modal = $('.event-detail-modal'),
				isModalDirty = false,
				username;

			onModalStateChange(modal, function (isModalVisible) {
				if(isModalVisible) {
					if(isModalDirty) {
						cleanPreviousEventData();
					}

					if(!isEditable) {
						username = element.attr('data-organisator');

						loadGalleries(username)
							.then(function () {
								onGalleryListChange();
							})
							.fail(function (error) {
								console.log(error);
							});	
					}	else {
						onGalleryListChange();
					}
				} else {
					isModalDirty = true;	
				}
			});

			if(isEditable) {
				username = $('#user-name').text();
				loadGalleries(username);
			}

			function cleanPreviousEventData() {
				element.chosen().val('').trigger('chosen:updated');
				coverPhotoSelect.chosen().val('').trigger('chosen:updated');
			}

			function loadGalleries(username) {
				var deferred = Q.defer();

				getUserGalleries(username)
					.then(function (data) {
						if(data.length != 0) {
							if(isEditable) {
								var galleryCountScope = $('#gallery-count-label').scope();

								setGalleries(galleryCountScope, data);
							}

							setGalleries(scope, data);
							populateSelectOptions(element, scope.galleries, 'title');
							deferred.resolve();
						}
					})
					.fail(function (error) {
						console.log(error);
						deferred.reject(error);
					});

				return deferred.promise;
			}

			function setGalleries(scope, galleries) {
				if(!scope.$$phase) {
					scope.$apply(function () {
						scope.galleries = galleries;
					});
				} else {
					scope.galleries = galleries;
				}
			}
		
			function onGalleryListChange() {
				var currentGallery = element.attr('data-gallery');
				
				if(currentGallery) {
					element.chosen().val(currentGallery).trigger('chosen:updated');
					onChange(currentGallery);
				}
					
				element.chosen().change(function (selected) {
					handleSelectListChange(selected, element, onChange, onNoneSelection);
				});

				function onChange(selected){
					filterElems(selected, scope.galleries, ['title'], false, true)
						.done(function (gallery) {
							gallery = gallery[0];

							setupCoverPhotoAndGallery(true);
							setGallery(gallery);

							populateSelectOptions(coverPhotoSelect, gallery.photos, 'title', true);
							onPhotoListChange(coverPhotoSelect, gallery.photos);
							coverPhotoSelect.trigger("chosen:updated");

							setupCoverPhotoAndGallery(true, false);
						});
				}

				function onNoneSelection(){
					setupCoverPhotoAndGallery(false, false);
				}

				function setGallery(gallery) {
					if(!scope.$$phase) {
						scope.$apply(function () {
							scope.gallery = gallery;
						});
					} else {
						scope.gallery = gallery;
					}
				}
			}

			function onPhotoListChange(photoSelect, photos) {
				var	coverPhoto = $('#event-detail-cover-photo'),
					currentCoverPhoto = photoSelect.attr('data-cover-photo');
				
				if(currentCoverPhoto) {
					photoSelect.chosen().val(currentCoverPhoto).trigger('chosen:updated');
					onChange(currentCoverPhoto);
				}

				photoSelect.chosen().change(function (selected) {
					handleSelectListChange(selected, photoSelect, onChange, onNoneSelection);
				});

				function onChange(selected){
					filterElems(selected, photos, ['title'], false, true)
						.done(function (photo) {
							photo = photo[0];
							if(photo) {
								setCoverPhoto(photo);
								setupCoverPhotoAndGallery(true, true);
							}
						});
				}

				function onNoneSelection(){
					setupCoverPhotoAndGallery(true, false);
				}

				function setCoverPhoto(photo) {
					if(!scope.$$phase) {
						scope.$apply(function () {
							scope.coverPhoto = photo;
						});
					} else {
						scope.coverPhoto = photo;
					}
				}
			}
		}
	}
}

function photoSelectDirective() {
	return {
		link: function(scope, element, attrs) {
			element = convertElemToJq(element);
		}
	}
}

function editableDetailDirective() {
	return {
		link: function(scope, element, attrs) {
			element = convertElemToJq(element);
			var isEditableMode = document.location.hash.contains('profile/organisator'),
				siblings = element.siblings(),
				linkedSibling = findElemsInCollectionByAttr('non-editable-detail', '', siblings)[0];

			makeHidden(element);

			if(linkedSibling) {
				element.watch('value', function(propName, oldVal, newVal){
					syncData();
				});

				if(isEditableMode) {
					element.hover(
						function() {
							element.removeAttr('data-mouse-out-of-editable-sibling');
						},
						function() {
							makeHidden(element);
							makeUnhidden(linkedSibling);
							element.attr('data-mouse-out-of-editable-sibling', 'true');
						}
					);
				}
			}

			function syncData() {
				var currentValue = element.val();
				if(linkedSibling.text() != currentValue) {
					linkedSibling.text(currentValue);
				}
			}
		}
	}	
}

function nonEditableDetailDirective() {
	return {
		link: function(scope, element, attrs) {
			element = convertElemToJq(element);
			var isEditableMode = document.location.hash.contains('profile/organisator'),
				siblings = element.siblings(),
				linkedSibling = findElemsInCollectionByAttr('editable-detail', '', siblings)[0];

			makeUnhidden(element);

			if(linkedSibling) {
				element.watch('innerHTML', function(propName, oldVal, newVal){
					syncData();
				});

				if(isEditableMode) {
					element.hover(
						function() {
							makeHidden(element);
							makeUnhidden(linkedSibling);
						},
						function() {
							setTimeout(function () {
								if(linkedSibling.attr('data-mouse-out-of-editable-sibling') == 'true') {
									makeHidden(linkedSibling);
									makeUnhidden(element);
								}
							}, 200);	
						}
					);
				}
			}

			function syncData() {
				var currentValue = element.text();
				if(linkedSibling.val() != currentValue) {
					linkedSibling.val(currentValue);
				}
			}
		}
	}
}

function organisatorProfileSpecificDirective() {
	return {
		link: function(scope, element, attrs) {
			element = convertElemToJq(element);
			var isEditableMode = document.location.hash.contains('profile/organisator');
			
			if(isEditableMode) {
				makeUnhidden(element);
			} else {
				makeHidden(element);
			}
		}
	}
}


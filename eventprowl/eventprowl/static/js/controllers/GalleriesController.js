function GalleriesController($scope, $rootScope, $compile, $route, $routeParams){
	console.log(2);

	if($rootScope.userType == '') {
		$rootScope.userType = $rootScope.userTypes.organisator;
		invertUserBoundNavigationElementsVisibility($rootScope.userType);
	}

	$scope.$on('$viewContentLoaded', function(){
		if(!$scope.$$phase) {
			$scope.$apply(function () {
				processContainedLinks();
			});
		} else {
			processContainedLinks();
		}
	});

	var currentMessagesWidgetParent;

	$('#main-messages-widget-container').waitUntilExists(function () {
		 currentMessagesWidgetParent = $('#main-messages-widget-container');
	});

	$('#photo-creation-messages-widget-container').waitUntilExists(function () {
		$('.add-photo-modal').waitUntilExists(function () {
			var createPhotoModal = $(this),
				photoCreationMessagesWidgetParent  = $('#photo-creation-messages-widget-container');

			attachMessagesWidgetTransporter(createPhotoModal,
													currentMessagesWidgetParent,
													photoCreationMessagesWidgetParent);

			$("#photo-creation-form").waitUntilExists(function () {
				var gallery_id = $('#gallery-header').attr('data-id'),
					extra_data = {
						gallery_id : gallery_id,
					};

				$(this).submit(function() {
					commonFormRequest(this, extra_data)
						.then(function() {
							closeModal(createPhotoModal);
						})
						.then(function () {
							if(createPhotoModal.data != undefined) {
								if(!createPhotoModal.data('bs.modal').isShown) {
									if(!$scope.$$phase) {
										$scope.$apply(function () {
											reloadCurrentView($scope, $compile, $route, $routeParams);
										});
									} else {
										reloadCurrentView($scope, $compile, $route, $routeParams);
									}
								}
							}
						})
						.fail(function (error) {
							console.log(error);
						});

					return false;
				});
			});
		});
	});

	$('#photo-removal-messages-widget-container').waitUntilExists(function () {
		var photoRemovalModal = $($('.remove-photo-modal')[0]),
			photoRemovalMessagesWidgetParent  = $('#photo-removal-messages-widget-container'),
			removalForm = photoRemovalModal.find('#photo-removal-form'),
			galleryRedirectButton = $('#gallery-redirect-button');

		attachMessagesWidgetTransporter(photoRemovalModal,
											currentMessagesWidgetParent,
								  		photoRemovalMessagesWidgetParent);

		removalForm.submit(function() {
			commonFormRequest(this)
				.then(function() {
					closeModal(photoRemovalModal);
				})
				.then(function () {
					if(!photoRemovalModal.data('bs.modal').isShown) {
						galleryRedirectButton.click();
					}
				})
				.fail(function (error) {
					console.log(error);
				});

			return false;
		});
	});

	$('#gallery-removal-messages-widget-container').waitUntilExists(function () {
		var galleryRemovalModal = $($('.remove-gallery-modal')[0]),
			galleryRemovalMessagesWidgetParent  = $('#gallery-removal-messages-widget-container'),
			removalForm = galleryRemovalModal.find('#gallery-removal-form'),
			galleriesRedirectButton = $('#galleries-redirect-button');

		attachMessagesWidgetTransporter(galleryRemovalModal,
									 	currentMessagesWidgetParent,
								  		galleryRemovalMessagesWidgetParent,
									   	$scope);

		removalForm.submit(function() {
	  		commonFormRequest(this)
	  			.then(function() {
					closeModal(galleryRemovalModal);
				})
	  			.then(function () {
	  				if(!galleryRemovalModal.data('bs.modal').isShown) {
	  					galleriesRedirectButton.click();
					}
				})
	  			.fail(function (error) {
					console.log(error);
				});

	  		return false;
		});
	});
}

function ListMessages($scope){
	$scope.getMessages = function() {
		var method = 'GET',
			$rootScope = getRootScope(),
			url = $rootScope.messagesUrl,
			deferred = Q.defer();

		makeRequestWithAjax(url, method)
			.then(function (data) {
				$scope.$apply(function () {
					$scope.messages_list = data;
					deferred.resolve();
				});
			})
			.fail(function(error) {
				deferred.reject(error);
			});

		return deferred.promise;
	};

	$scope.clearMessage = function(msg) {
		var index = $scope.messages_list.indexOf(msg);
		if (index > -1) {
			$scope.messages_list.splice(index, 1);
		}
	}

	$scope.hasMessages = function() {
		return $scope.messages_list.length > 0;
	}
}

function MainController($scope){
	console.log(1);

	invertSearchElementsVisibility();
	$scope.$on('$destroy', function () {
		invertSearchElementsVisibility();
	})
		
	var $rootScope = getRootScope();

	$scope.eventsDataUrl = $rootScope.eventsDataUrl;
	$scope.numPerPage = 10;

	refreshEventsData($scope)
		.then(function() {
			setupEvents($scope);
		})
		.then(function() {
			$rootScope.$broadcast('$eventsLoaded');
		})
		.fail(function (error) {
			console.log(error);
		});
}

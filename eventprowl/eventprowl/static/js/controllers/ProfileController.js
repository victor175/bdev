function ProfileController($scope, $routeParams) {
	console.log(42);

	commonProfileHandler($scope, $routeParams);
	
	var $rootScope = getRootScope(),
		userType = $rootScope.userType,
		userTypes = $rootScope.userTypes;

	invertSearchElementsVisibility();
	$scope.$on('$destroy', function () {
		invertSearchElementsVisibility();
	})

	if(userType == userTypes.organisator) {
		handleOrganisatorProfile($scope);
	} else if (userType == userTypes.observer) {
		handleObserverProfile($scope);
	}
}

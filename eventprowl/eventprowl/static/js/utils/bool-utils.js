function isPartialNameBoundToOtherUserType(userType, partialName) {
	var organisatorSpecificElements = ['gallery'],
		observerSpecificElements = [],
		elementsToBeChecked,
		$rootScope = getRootScope(),
		userTypes = $rootScope.userTypes,
		userType = $rootScope.userType,
		isBound;

	if(userType == userTypes.organisator) {
		elementsToBeChecked = observerSpecificElements;
	} else if (userType == userTypes.observer) {
		elementsToBeChecked = organisatorSpecificElements;
	} else {
		return -1;
	}

	isBound = isElemContained(elementsToBeChecked, partialName);

	return isBound;
}

function isElemContained(collection, elem) {
	var index = collection.indexOf(elem),
		isContained = (index == -1) ? false : true;

	return isContained;
}

function isMenuLinkBoundToOtherUserType(userType, menuLink) {
	var partialName = convertMenuLinkToPartialName(menuLink),
		isUserBound = isPartialNameBoundToOtherUserType(userType, partialName);

	return isUserBound;
}

function shouldInvertMenuLinkVisibility(menuLink, userType) {
	var isUserBound = isMenuLinkBoundToOtherUserType(userType, menuLink),
		shouldInvert = isUserBound ? false : true;

	return shouldInvert;
}

function isElemPropValMatching(match, elem, properties, shouldBeFullMatch) {
	shouldBeFullMatch = (typeof shouldBeFullMatch === "undefined") ? false : shouldBeFullMatch;
	
	var isAnyMatching = false;

	if(properties.length == 0) {
		properties = Object.keys(elem);
	}

	for(var key in properties) {
		var	prop = properties[key],
			value = elem[prop];
		if(value != undefined) {
			value = value + "";
			if(shouldBeFullMatch) {
				isAnyMatching = (value === match);
			} else {
				isAnyMatching = value.contains(match);
			}

			if(isAnyMatching) {
				break;
			}
		}
	}

	return isAnyMatching;
}

function isMessagesListEmpty() {
    var scope = angular.element($('#messages-list')).scope(),
    		isEmpty;

	if(scope != undefined) {
		isEmpty = !scope.hasMessages();
	} else {
		isEmpty = true;
	}
	return isEmpty;
}

function isDomElem(obj) {
	return (obj.tagName ? true : false);
}

function isHidden(elem) {
	elem = convertElemToJq(elem);	
	return elem.is(":visible") == true;
}



function getElemClasses(elem) {
	elem = convertElemToJq(elem);

	return elem.attr('class').split(' ');
}

function convertElemToJq(elem) {
	if(!(elem instanceof jQuery)) {
		elem = $(elem);
	}

	return elem;
}

function findElemsInCollectionByAttr(attr, value, collection) {
	var elems = [];	

	for(var i = 0; i < collection.length; i++) {
		elem = $(collection[i]);
		if(elem.attr(attr) === value) {
			elems.push(elem);
		}
	}

	return elems;
}

function getDomElemsInContainer(container, tag) {
	var elems = container.find(tag),
		elem;

	for(var i = 0; i < elems.length; i++) {
		elem = elems[i];
		
		if(!isDomElem(elem)) {
			removeObjFromCollection(i, elems);
		} else {
			elems[i] = convertElemToJq(elem);	
		}
	}

	return elems;
}

function processContainedLinksHref(container) {
	var links = getDomElemsInContainer(container, 'a');

	for(var i = 0; i < links.length; i++) {
		link = links[i];

		href = link.attr('href');
		href = processPhotologueUrl(href);

		if(href != -1) {
			link.attr('href', href);
		}
	}
}

function processContainedLinks() {
	var	root = $('#root');

	processContainedLinksHref(root);
}

function changeParent(currentParent, newParent, position) {
	if(currentParent != undefined && newParent != undefined) {
		var child = $(currentParent.children()[position]);
		if(child != undefined) {
			newParent.append(child);
		}
	}
}

function attachMessagesWidgetTransporter(modal, currentParent, possibleNewParent) {
	onModalStateChange(modal, function (isModalVisible) {
		transportMessagesWidget(isModalVisible, currentParent, possibleNewParent);
	});
}

function transportMessagesWidget(isModalVisible, currentParent, possibleNewParent) {
	var oldMessagesWidgetParent,
		newMessagesWidgetParent;

	if(isModalVisible) {
		oldMessagesWidgetParent = currentParent;
		newMessagesWidgetParent = possibleNewParent;
	} else {
		oldMessagesWidgetParent = possibleNewParent;
		newMessagesWidgetParent = currentParent;
	}

	updateMessagesList();
	changeParent(oldMessagesWidgetParent, newMessagesWidgetParent, 0);
}

function onModalStateChange(modal, callback) {
	var changeListenerInterval = setInterval(watch(modal.get(0), "clientHeight", function () {
		callback(modal.is(':visible'), changeListenerInterval);
	}), 500);	
}

function addOptionToSelect(select, option) {
	var optionHTML = '<option>' + option + '</option>'; 
	select.append(optionHTML);
}

function clearSelectOptions(select) {
	var lastOption = select.children().last();

	while(lastOption.attr('value') != 'None') {
		lastOption.remove();
		
		if(select.children().length > 0) {
			lastOption = select.children().last();
		} else {
			break;
		}
	}
}

function populateSelectOptions(select, options, attribute, clearCurrent) {
	attribute = (typeof attribute === "undefined") ? false : attribute;
	clearCurrent = (typeof clearCurrent === "undefined") ? false : clearCurrent;
	var option;

	if(clearCurrent) {
		clearSelectOptions(select);
	}

	for(key in options) {
		option = options[key];
		if(attribute) {
			option = option[attribute];
		}

		addOptionToSelect(select, option);
	}
}

function handleSelectListChange(selected, select, onChangeCallback, onNoneSelectionCallback) {
	var selectedValue = $(selected.target).val(),
		noSelectionValue = "None";
	
	if(selectedValue === noSelectionValue) {
		onNoneSelectionCallback();
	} else {
		onChangeCallback(selectedValue);
	}
}

function findSearchFilterId(name) {
	var searchFiltersList = $("#search-filters-list > li > a"),
		searchFiltersListIds = [],
		filter;	

	for(key in searchFiltersList) {
		filter = $(searchFiltersList[key]);
		filterName = filter.text()

		if(filterName === name) {
			id = filter.attr('id')	
			return id;
		}
	}
}

function getCurrentImagesCount() {
	var holders = $('.images-holder'),
		holder,
		imagesCount = 0;

	for(var i = 0; i < holders.length; i++) {
		holder = $(holders[i]);
		imagesCount += holder.find('a').length;
	}

	return imagesCount;
}

function findEventElemOnPage(title) {
	var deferred = Q.defer();

	$('.event-list > .event-element').waitUntilExists(function () {
		var list = $(this),
			scope,
			event;

		for(var i = 0; i < list.length; i++) {
			event = $(list[i]);
			scope = event.scope();

			if(title == scope.event.title) {
				deferred.resolve(event);
			}
		}

		deferred.reject();
	});

	return deferred.promise;
}

function openEventDetails(title) {
	findEventElemOnPage(title)
		.then(function (event) {
			eventTitle = event.find('.info > .title');
			eventTitle.click();
		})
		.fail(function (err) {
			console.log(err);
		});
}

function handleOrganisatorProfile($scope) {
	var currentMessagesWidgetParent = $('#main-messages-widget-container');
	$scope.galleries = []

	$('.add-event-modal').waitUntilExists(function () {
		var createEventModal = $(this),
			eventCreationMessagesWidgetParent  = $('#event-creation-messages-widget-container');

		attachMessagesWidgetTransporter(createEventModal,
											currentMessagesWidgetParent,
											eventCreationMessagesWidgetParent);

		$("#event-creation-form").waitUntilExists(function () {
			$(this).submit(function() {
				commonFormRequest(this)
					.then(function() {
						closeModal(createEventModal);
					})
					.then(function () {
						if(!createEventModal.data('bs.modal').isShown) {
							fetchEvents($scope);
						}
					})
					.fail(function (error) {
							console.log(error);
					});	

				return false;
			});
		});

		$('#event-creation-form-field3').waitUntilExists(function () {
			$(this).datetimepicker();
		});

		$('#event-creation-form-field4').waitUntilExists(function () {
			$(this).datetimepicker();
		});	
	});

	$('.add-gallery-modal').waitUntilExists(function () {
		var createGalleryModal = $(this),
			galleryCreationMessagesWidgetParent  = $('#gallery-creation-messages-widget-container');

		attachMessagesWidgetTransporter(createGalleryModal,
											currentMessagesWidgetParent,
											galleryCreationMessagesWidgetParent);
											
		$("#gallery-creation-form").waitUntilExists(function () {
				$(this).submit(function() {
					commonFormRequest(this)
						.then(function() {
							closeModal(createGalleryModal);
						})
						.then(function (data) {
							if(!createGalleryModal.data('bs.modal').isShown) {
								redirectToLocation(getRootUrl() + '#/gallery');
							}
						})
						.fail(function (error) {
							console.log(error);
						});

					return false;
				});
			});
	});

	$('.event-detail-modal').waitUntilExists(function () {
		var eventDetailModal = $(this),
			eventDetailMessagesWidgetParent = $('#event-detail-messages-widget-container');

		attachMessagesWidgetTransporter(eventDetailModal,
												currentMessagesWidgetParent,
												eventDetailMessagesWidgetParent);

		$('#event-detail-form').waitUntilExists(function () {
			$(this).submit(function() {
				var	eventTitleElement = $(this).find('.modal-title'),
					oldEventTitle = eventTitleElement.attr('data-old-title'),
					gallerySelectScope = $('#select-gallery').scope(),
					gallery = gallerySelectScope.gallery,
					extraData = {
						old_title: oldEventTitle,
						gallery_title: gallery != undefined ? gallery.title : '',
					};
				
				commonFormRequest(this, extraData)
					.then(function() {
						closeModal(eventDetailModal);
					})
					.then(function () {
						if(!eventDetailModal.data('bs.modal').isShown) {
							refreshEventsData($scope)
								.then(function() {
									setupEvents($scope);
								})
								.fail(function (error) {
									console.log(error);
								});
						}
					})
					.fail(function (error) {
						console.log(error);
					});
				
				return false;
			});
		});
	});
}

function handleObserverProfile($scope) {
	
}

function commonProfileHandler($scope, $routeParams) {
	var $rootScope = getRootScope(),
		requestData = $rootScope.requestData;

	if($rootScope.userType == '') {
		$rootScope.userType = $routeParams.type;
		invertUserBoundNavigationElementsVisibility($rootScope.userType);
	}

	if(requestData != undefined) {
		setRequestData(requestData);
	}

	$scope.eventsDataUrl = $rootScope.getEventsDataUrl($scope.requestData.username);
	$scope.numPerPage = 5;
	
	refreshEventsData($scope)
		.then(function() {
			setupEvents($scope);
		})
		.fail(function (error) {
			console.log(error);
		});
	
	$('.remove-event-modal').waitUntilExists(function () {
		var removeEventModal = $(this),
			currentMessagesWidgetParent = $('#main-messages-widget-container'),
			eventRemovalMessagesWidgetParent = $('#event-removal-messages-widget-container');

		attachMessagesWidgetTransporter(removeEventModal,
										currentMessagesWidgetParent,
										eventRemovalMessagesWidgetParent);
	});

	function setRequestData(data) {
		if(!$scope.$$phase) {
			$scope.$apply(function () {
				$scope.requestData = data;
			});
		} else {
			$scope.setRequestData = data;
		}
	}

}

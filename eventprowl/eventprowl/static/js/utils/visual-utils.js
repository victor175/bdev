function showPassword() {
	var elementContainingPass = $('input[name=password]'),
		key_attr = elementContainingPass.attr('type');
	
	if(key_attr != 'text') {
		$('.checkbox').addClass('show');
		elementContainingPass.attr('type', 'text');
	} else {
		$('.checkbox').removeClass('show');
		elementContainingPass.attr('type', 'password');
	}  
}

function invertUserBoundNavigationElementsVisibility(userType) {
	var boundLinksContainer = 'user-bound-links',
		container = $('#' + boundLinksContainer),
		contained = container.children();

	contained.each(function() {
		var link = $(this).find('a'),
			shouldInvertElemVisibility = shouldInvertMenuLinkVisibility(link, userType);

		if(shouldInvertElemVisibility) {
			invertElementVisibility(link);
		}
	});
}

function invertSearchElementsVisibility() {
	var searchWrapper = $('.search-wrapper');

	invertElementVisibility(searchWrapper);
	$('#search-input').val('');
}

function invertElementVisibility(elem) {
	var hiddenElementsClass = 'hide';
		elem = convertElemToJq(elem);

	if(elem.hasClass(hiddenElementsClass)) {
		elem.removeClass(hiddenElementsClass)
	} else {
		elem.addClass(hiddenElementsClass)
	}
}

function hide(elem) {
	var hiddenElementsClass = 'hide',
		elem = convertElemToJq(elem);

	if(!elem.hasClass(hiddenElementsClass)) {
		elem.addClass(hiddenElementsClass)
	}	
}

function unhide(elem) {
	var hiddenElementsClass = 'hide',
		elem = convertElemToJq(elem);

	if(elem.hasClass(hiddenElementsClass)) {
		elem.removeClass(hiddenElementsClass);
	}	
}

function makeHidden(elem) {
	var hiddenElementsClass = 'hidden',
		elem = convertElemToJq(elem);

	if(!elem.hasClass(hiddenElementsClass)) {
		elem.addClass(hiddenElementsClass)
	}	
}

function makeUnhidden(elem) {
	var hiddenElementsClass = 'hidden',
		elem = convertElemToJq(elem);

	if(elem.hasClass(hiddenElementsClass)) {
		elem.removeClass(hiddenElementsClass);
	}	
}

function closeModal(modal) {
	if(isMessagesListEmpty()) {
		modal.modal('hide');
	}
}

function scrollToBottom() {
	$("html, body").animate({
		scrollTop: $(document).height()
	}, 1200);
}

var activatePageLink = function(elem, $rootScope) {
	elem = convertElemToJq(elem);

	visualMenuLinkDeactivator($rootScope);
	visualMenuLinkActivator(elem, $rootScope);
}

var visualMenuLinkActivator = function(elem, $rootScope) {
	$rootScope = (typeof $rootScope === "undefined") ? getRootScope() : $rootScope;

	var parent = elem.parent();

	parent.addClass($rootScope.activateClassString);
	$rootScope.lastActivated = parent;
}

var visualMenuLinkDeactivator = function($rootScope) {
	$rootScope = (typeof $rootScope === "undefined") ? getRootScope() : $rootScope;
	elem = $rootScope.lastActivated;

	if(elem) {
		elem.removeClass($rootScope.activateClassString);
	}
}

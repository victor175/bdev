var redirectToCorrespondingView = function (callerElem) {
	var newLocation = getNewLocation(callerElem);
	redirectToLocation(newLocation);
}

function redirectToLocation(location) {
	if(location != -1) {
		window.location.replace(location);
	}
}

function logout(username) {
	var	$rootScope = getRootScope(),
		logoutUrl = $rootScope.logoutUrl,
		method = 'POST';

	invertUserBoundNavigationElementsVisibility($rootScope.userType);
	$rootScope.userType = '';
	
	makeRequestWithAjax(logoutUrl, method)
		.then(function (data) {
			if(data != '') {
				redirectToLocation(data);
				cleanCookies();			
			}
		});
}

function getUserGalleries(username) {
	var deferred = Q.defer(),
		$rootScope = getRootScope(),
		url = $rootScope.getGalleriesUrl(username),
		method = 'GET';

	makeRequestWithAjax(url, method)
		.then(function (data) {
			deferred.resolve(data)
		})
		.fail(function (error) {
			deferred.reject(error);
		});

	return deferred.promise;
}

function cleanCookies() {
	angular.forEach($cookies, function (v, k) {
		$cookieStore.remove(k);
	});
}

function changePageLanguage(newLanguage) {
	var $rootScope = getRootScope(),
		newLanguage = $rootScope.languages[newLanguage];
	
	reloadCurrentView(undefined, undefined, undefined, undefined, newLanguage);
}

function reloadCurrentView($scope, $compile, $route, $routeParams, newLanguage) {
	if(newLanguage != undefined) {
		var currentUrl = window.location.href;
		currentUrl = changeLanguageInUrl(currentUrl, newLanguage);
		
		window.location.replace(currentUrl);
	} else if($route != undefined && $routeParams != undefined && $scope != undefined && $compile != undefined) {
		var currentPageTemplate = $route.current.templateUrl($routeParams),
			rootElem = $('#root');

		rootElem.load(currentPageTemplate);
		$compile(rootElem.contents())($scope);

		signalizeGalleryViewLoaded();
	}
}

function makeRequestWithAjax(url, method, data, extra_settings) {
	var settings = assembleAjaxSettings(url, method, data, extra_settings),
		req = $.ajax(settings);

	return Q(req);
}

function assembleAjaxSettings(url, method, data , extra_settings) {
	data = (typeof data === "undefined") ? undefined : data;
	extra_settings = (typeof extra_settings === "undefined") ? undefined : extra_settings;

	method = method.toLowerCase();
	var settings = {
			method: method,
			url: url,
		};

	settings = attachCSRFTokenToAjaxSettings(method, settings, data);

	if(extra_settings != undefined) {
		settings = $.extend(settings, extra_settings);
	}

	return settings;
}

function attachCSRFTokenToAjaxSettings(method, settings, data) {
	if(method == 'post' || method == 'put') {
		if(data != undefined) {
			settings.data = data; 
		} else {
			settings.data = {};
		}

		settings.data.csrfmiddlewaretoken = $.cookie('csrftoken');
	}

	return settings;
}

function commonFormRequest(form, extra_data) {
	extra_data = (typeof extra_data === "undefined") ? {} : extra_data;
	$form = convertElemToJq(form);

	var deferred = Q.defer(),
		method = $form.attr('method'),
		settings = {};

	settings.success = function (data) {
		if(data == '') {
			updateMessagesList()
				.then(function () {
					deferred.resolve();
				})
				.fail(function (error) {
					deferred.reject(error);
				});
		} else if (typeof data === "string") {
			redirectToLocation(data);
		} else {
			getRootScope().requestData = data;
			redirectToLocation(data['redirection_url']);
		}
	};

	settings.error = function (err) {
		deferred.reject(err);
	}

	attachCSRFTokenToAjaxSettings(method, settings, extra_data);
	
	$form.ajaxSubmit({
		data: settings.data,
		success: settings.success,
		error: settings.error,
	});

	return deferred.promise;
}

function fetchEvents($scope) {
	var deferred = Q.defer();
	refreshEventsData($scope)
		.then(function () {
			deferred.resolve();
		})
		.fail(function (error) {
			deferred.reject(error);
		});

	return deferred.promise;
}

function refreshEventsData($scope) {
	var method = "GET",
		url = $scope.eventsDataUrl,
		deferred = Q.defer();
	
	makeRequestWithAjax(url, method)
		.then(function (data) {
			$scope.$apply(function () {
				$scope.events = data;
			});
			deferred.resolve();
		})
		.fail(function(error) {
			console.log('err');
			deferred.reject(error);
		});

	return deferred.promise;
}

var getNewLocation = function(menuLink) {
	var profileLinkDefaultClass = 'profile',
		newLocation = window.location.href.split('#')[0] + '#',
		partialName = convertMenuLinkToPartialName(menuLink);
	
	if(partialName != -1) {
		newLocation += partialName;
	} else {
		newLocation = partialName;
	}

	return newLocation;
}

function getRootUrl() {
	var splitToken = '#',
		url = document.URL,
		rootUrl;

	if(url.contains(splitToken)) {
		rootUrl = url.split(splitToken)[0];
	} else {
		rootUrl = url;
	}

	addSuffix(rootUrl, '/');

	return rootUrl;
}

var getPartialsRootUrl = (function () {
	var partialsRoot = 'processed_partials/',
		partialsRootUrl = getRootUrl() + partialsRoot;

	return partialsRootUrl;
})();


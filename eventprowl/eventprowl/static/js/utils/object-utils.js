function filterElems(keyword, elements, propertiesToFilter, invertResult, shouldBeFullMatch) {
	shouldBeFullMatch = (typeof shouldBeFullMatch === "undefined") ? false : shouldBeFullMatch;
	invertResult = (typeof invertResult === "undefined") ? false : invertResult;
	var deferred = Q.defer(),
		loops = 0,
		filteredElems = [];
	
	elements.filter(function (elem) {
		var isMatching = isElemPropValMatching(keyword, elem, propertiesToFilter, shouldBeFullMatch);
		if(invertResult) {
			isMatching = !isMatching;
		}

		if(isMatching) {
			filteredElems.push(elem);
		}

		loops ++;

		if(loops == elements.length) {
			deferred.resolve(filteredElems);
		}

		return isMatching;
	});

	return deferred.promise;
}

function applySearchCriteria() {
	var keyword =  $('#search-input').val(),
		searchFilters = getProcessedSearchFilters(),
		loadedEvents = getLoadedEvents();

	filterElems(keyword, loadedEvents, searchFilters)
		.done(function (matchingEvents) {
			changeLoadedEvents(matchingEvents);
		});
}

function removeEventFromLoaded(eventTitle) {
	filterElems(eventTitle, getLoadedEvents(), ['title'], true, true)
		.done(function (loadedEvents) {
			changeLoadedEvents(loadedEvents, true);
		});
}

function incrementEventObservers(eventTitle) {
	var loadedEvents = getLoadedEvents(true);

	filterElems(eventTitle, loadedEvents, ['title'], false, true)
		.done(function (event) {
			event = event[0];
			var eventIndex = $.inArray(event, loadedEvents);

			if(eventIndex != -1) {
				event.observers += 1;
				loadedEvents[eventIndex] = event;
				changeLoadedEvents(loadedEvents);
			}
		});
}

function getLoadedEvents(onlyFiltered) {
	onlyFiltered = (typeof onlyFiltered === "undefined") ? false : onlyFiltered;

	var scope = angular.element($('#root')).scope(),
		events;

	if(scope != undefined) {
		events = onlyFiltered ? scope.filteredEvents : scope.events;
	} else {
		events = undefined;
	}

	return events;
}

function changeLoadedEvents(newEvents, shouldChangeBaseEvents) {
	shouldChangeBaseEvents = (typeof shouldChangeBaseEvents === "undefined") ? false : shouldChangeBaseEvents;
	var scope = angular.element($('#root')).scope();

	if(scope != undefined) {
		if(shouldChangeBaseEvents) {
			scope.updateAndApplyEventsVal(newEvents);	
		} else {
			scope.updateAndApplyFilteredEventsVal(newEvents);	
		}
	}
}

function updateMessagesList() {
	var scope = angular.element($('#messages-list')).scope(),
		deferred = Q.defer();

	if(scope != undefined) {
		if(scope.getMessages == undefined) {
			ListMessages(scope);
		}

		scope.getMessages()
			.then(function () {
				deferred.resolve();
			})
			.fail(function (error) {
				deferred.reject(error);
			})
	}

	return deferred.promise;
}

function generateNewMessage(text, status) {
	var scope = angular.element($('#messages-list')).scope(),
		message = {
			text: text,
			status: status,
		}
		
	if(scope != undefined) {
		if(!scope.$$phase) {
			scope.$apply(function () {
				scope.messages_list.push(message);
			});
		} else {
			scope.messages_list.push(message);
		}
	}
}

function removeObjFromCollection(index, collection) {
	return collection.splice(index, 1);
}

function getRootScope() {
	return angular.element('html').scope();
}

function setupEvents($scope) {
	$scope.currentPageEvents = [],
		$scope.filteredEvents = $scope.events,
		$scope.currentPage = 1,
		$scope.maxSize = 5,
		$rootScope = getRootScope()
		$scope.eventsViewUrl = $rootScope.eventsViewUrl,
		$scope.eventShortTemplateUrl = $rootScope.eventShortTemplateUrl;

	$scope.numPages = function() {
		return Math.ceil($scope.filteredEvents.length / $scope.numPerPage);
	};

	$scope.hasEvents = function() {
		var currentPageEventsCount = $scope.currentPageEvents.length,
			firstEvent,
			hasEvents,
			isJson;

		if(currentPageEventsCount > 0 && $scope.filteredEvents != undefined) {
			firstEvent = $scope.filteredEvents[0];
			if(typeof firstEvent != 'object'){
				hasEvents = false;
			} else {
				hasEvents = true;
			}
		} else {
			hasEvents = false;
		}

		return  hasEvents;
	};

	$scope.getEventObserversCount = function() {
		var observersCount = 0;

		for(key in $scope.events) {
			observersCount += $scope.events[key].observers;
		}

		return observersCount;
	};

	$scope.goToLastPage = function() {
		$scope.data.currentPage = $scope.numPages(); 
	}

	$scope.updateFilteredEvents = function() {
		var begin = (($scope.currentPage - 1) * $scope.numPerPage),
			end = begin + $scope.numPerPage;

		$scope.currentPageEvents = $scope.filteredEvents.slice(begin, end);
	}

	$scope.updateAndApplyFilteredEvents = function() {
		if(!$scope.$$phase) {
			$scope.$apply(function () {
				$scope.updateFilteredEvents();
			});
		} else {
			$scope.updateFilteredEvents();
		}
	}

	$scope.updateAndApplyFilteredEventsVal = function (newEvents) {
		if(!$scope.$$phase) {
			$scope.$apply(function () {
				$scope.filteredEvents = newEvents;
			});
		} else {
			$scope.filteredEvents = newEvents;
		}
	}

	$scope.updateAndApplyEventsVal = function (newEvents) {
		if(!$scope.$$phase) {
			$scope.$apply(function () {
				$scope.events = newEvents;
			});
		} else {
			$scope.events = newEvents;
		}
	}

	$scope.updateAndApplyObserversCount = function (newCount) {
		if(!$scope.$$phase) {
			$scope.$apply(function () {
				$scope.observersCount = newCount;
			});
		} else {
			$scope.observersCount = newCount;
		}
	}

	$scope.data = {currentPage: $scope.currentPage};

	$scope.$watch('data.currentPage', function() {
		$scope.currentPage = $scope.data.currentPage;

		$scope.updateAndApplyFilteredEvents();
	});

	$scope.$watch('filteredEvents', function () {
		$scope.updateAndApplyFilteredEvents();
	}, true);

	$scope.$watch('events', function () {
		$scope.updateAndApplyEventsVal($scope.events);
		$scope.updateAndApplyFilteredEventsVal($scope.events);
		$scope.updateAndApplyObserversCount($scope.getEventObserversCount());
	}, true);

	$scope.updateAndApplyFilteredEvents();

	return $scope;
}

function watch(obj, prop, handler) {
	var currval = obj[prop];

	function callback() {
		if (obj[prop] != currval) {
			var temp = currval;

			currval = obj[prop];
			handler();
		}
	}

	return callback;
}

function moveEventToFirstPos(title) {
	var events = getLoadedEvents(),
		deferred = Q.defer();

	filterElems(title, events, ['title'], false, true)
		.then(function (demandedEvent) {
			demandedEvent = demandedEvent[0];
			var demandedEventPos = events.indexOf(demandedEvent)

			events.splice(demandedEventPos, 1);
			events.unshift(demandedEvent);

			changeLoadedEvents(events, true);

			deferred.resolve();
		})
		.fail(function (err) {
			deferred.reject(err);	
		});
	
	return deferred.promise;
}

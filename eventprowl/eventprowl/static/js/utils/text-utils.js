var separateLinkClass = function(classes) {
	var linkClass;
	if(classes.length > 1) {
		for(var i = 0; i < classes.length; i++) {
			if(classes[i].indexOf('link') > -1) {
				linkClass = classes[i];
				break;
			}
		}
	} else if (classes.length == 1) {
		linkClass = classes[0];
	} else {
		linkClass = -1;
	}
	
	return linkClass;
}

function addSuffix(str, suffix) {
	var lastCharIndex = str.length - 1,
		lastChar;

	if(lastCharIndex != -1) {
		lastChar = str[lastCharIndex];

		if(lastChar != suffix) {
			str += suffix;
		}
	}

	return str;
}

function addPrefix(str, prefix) {
	if(str.indexOf(prefix) != 0) {
		str = prefix + str;
	} else {
		str = -1;
	}

	return str;
}

function removeTextFromStr(str, text) {
	var regex = new RegExp(text, 'g'),
		res = str.replace(regex, '');

	return res;
}

function extractPartialNameFromDemandedRes(demandedRes) {
	var homePartialAddr='#/',
		partialNameSeparators = ['#/', '#'],
		partialNameSeparator = '',
		partialName;

	if(demandedRes == homePartialAddr || demandedRes == '') {
		partialName = 'home';
	} else {
		for(key in partialNameSeparators) {
			if(location.hash.indexOf(partialNameSeparators[key]) != -1) {
				partialNameSeparator = partialNameSeparators[key];
				break;
			}
		}

		partialName = location.hash.split(partialNameSeparator)[1];

		if(partialName.contains('/')) {
			partialName = partialName.split('/')[0];
		}
	}

	return partialName;
}

function convertPartialNameToMenuLink(partialName) {
	var menuLinkClass = '.' + partialName + '-link',
		menuLink = $(menuLinkClass);

	return menuLink;
}

function convertMenuLinkClassToPartialName(menuLinkClass) {
	if(menuLinkClass != -1 && menuLinkClass != undefined) {
		partialName = menuLinkClass.split('-')[0];
	} else {
		partialName = -1;
	}

	return partialName;
}

function convertMenuLinkToPartialName(menuLink) {
	var classes = getElemClasses(menuLink),
		menuLinkClass = separateLinkClass(classes),
		partialName = convertMenuLinkClassToPartialName(menuLinkClass);

	return partialName;
}

function formatAsEventAttr(str) {
	str = str.toLowerCase();
	str = str.replace(' ', '_');
	str = str.replace('-', '_');

	return str;
}

function formatFilterIdAsEventAttr(id) {
	id = id.split('-')
	id = id[id.length - 1]

	return id;
}

function createCustomDateTimeObject(timestamp) {
	var datetime = moment(timestamp),
		month = datetime.format('MMM'),
		day = datetime.format('D'),
		year = datetime.format('YYYY'),
		time = datetime.format('hh:mm A'),
		datetimeObj = {
			year: year,
			month: month,
			day: day,
			time: time,
		};

	return datetimeObj;
}

function changeLanguageInUrl(url, newLanguage) {
	newLanguage += "/"

	var rootUrl = getRootUrl(),
		changedRootUrl = rootUrl.substring(0, rootUrl.length - newLanguage.length);
	
	changedRootUrl += newLanguage,
	url = url.replace(rootUrl, changedRootUrl);

	return url;
}

function getProcessedSearchFilters() {
	var searchFiltersAsText = $('#search-filters').text(),
		searchFilters = searchFiltersAsText.split(', '),
		searchFilterDefault = $('#search-by-everything').text(),
		filterId;

	searchFilters = searchFilters.filter(function (elem) {
		return elem != searchFilterDefault;
	});

	for(var i = 0; i < searchFilters.length; i++) {
		filterId = findSearchFilterId(searchFilters[i]);
		filterId = formatFilterIdAsEventAttr(filterId);

		searchFilters[i] = filterId;
	}

	return searchFilters;
}

function processPhotologueUrl(url) {
	var prefix = '#',
		$rootScope = getRootScope(),
		currentLang = $rootScope.currentLang,
		unwantedUrlPart = '/' + currentLang + '/photologue';
	
	if(url.indexOf(unwantedUrlPart) != 0) {
		url = -1;
	} else {
		url = removeTextFromStr(url, unwantedUrlPart);
		url = addPrefix(url, prefix);
	}

	return url;
}

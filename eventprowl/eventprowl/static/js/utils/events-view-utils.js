function initShortEventHolder(element, scope){
	var	$rootScope = getRootScope(),
		eventDetailModal = $('.event-detail-modal'),
		eventHolderTimeHolder = element.find('.event-time-holder'),
		eventHolderTitle = element.find('.info .title'),
		eventHolderDesc = element.find('.info .desc'),
		eventHolderCoverPhoto = element.find('.event-cover-photo'),
		eventHolderShareClickables = element.find('.share-event'),
		modalTitle = eventDetailModal.find('.modal-title'),
		modalDescriptionSection = eventDetailModal.find('#event-description-section'),
		modalDesc = modalDescriptionSection.find('#event-description-editable'),
		modalDescNonEditable = modalDescriptionSection.find('#event-description-non-editable'),
		modalGalleryHolder = eventDetailModal.find('#event-gallery'),
		modalNoGalleryFallback = eventDetailModal.find('#no-gallery-fallback'),
		modalCoverPhotoSection = eventDetailModal.find('#cover-photo-section'),
		modalCoverPhotoHolder = modalCoverPhotoSection.find('#event-cover-photo'),
		modalNoCoverPhotoFallback = modalCoverPhotoSection.find('#no-cover-photo-fallback'),
		modalStartDate = eventDetailModal.find('#event-start-date-editable'),
		modalEndDate = eventDetailModal.find('#event-end-date-editable'),
		modalSelectCoverPhotoButton = modalCoverPhotoSection.find('#select-cover-photo'),
		event = scope.event,
		isEditable = document.location.hash.contains('profile/organisator'),
		shareEventModal = $('.share-event-modal'),
		shareEventModalTitle = shareEventModal.find('.modal-title'),
		shareEventModalUrlContainer = shareEventModal.find('#share-modal-url-container'),
		modalGallerySelect = eventDetailModal.find('#select-gallery');

	eventHolderTimeHolder
		.add(eventHolderCoverPhoto)
		.add(eventHolderTitle)
		.add(eventHolderDesc).click(function () {
		showEventDetailModal();
	});

	eventHolderShareClickables.click(function () {
		showEventShareModal();
	});

	if(event.cover_photo) {
		eventHolderCoverPhoto.attr('src', event.cover_photo.thumbnail_url);
		unhide(eventHolderCoverPhoto);
	} else {
		hide(eventHolderCoverPhoto);
	}

	modalStartDate.waitUntilExists(function () {
		$(this).datetimepicker();
	});

	modalEndDate.waitUntilExists(function () {
		$(this).datetimepicker();
	});

	function showEventShareModal() {
		var eventShareUrl = $rootScope.getEventShareUrl(event.title),
			initialTitleText = shareEventModalTitle.text();
		shareEventModal.modal('show');

		shareEventModalTitle.text(initialTitleText + ' ' + event.title);
		shareEventModalUrlContainer.text(eventShareUrl);
		shareEventModalUrlContainer.attr('href', eventShareUrl);

		onModalStateChange(shareEventModal, function (isVisible, changeListenerInterval) {
			if(!isVisible) {
				shareEventModalTitle.text(initialTitleText);
				clearInterval(changeListenerInterval);
			}
		});
	}

	function showEventDetailModal() {
		eventDetailModal.modal('show');

		modalTitle.text(event.title);
		modalTitle.attr('data-old-title', event.title);
		modalDesc.val(event.description);

		if(!isEditable) {
			modalGallerySelect.attr('data-organisator', event.organisator);
		}

		setupCoverPhotoAndGallery(event.gallery, event.cover_photo.thumbnail_url, event.cover_photo.title);

		modalStartDate.val(event.start_date);
		
		var endDateValue
		if (typeof event.end_date == 'string') {
			endDateValue  = event.end_date;
		} else {
			endDateValue = '';
		}

		modalEndDate.val(endDateValue);
	}
}

function setupCoverPhotoAndGallery(gallery, coverPhotoUrl, coverPhotoTitle) {
	var	eventDetailModal = $('.event-detail-modal'), 
		modalDescriptionSection = eventDetailModal.find('#event-description-section'),
		modalGalleryHolder = eventDetailModal.find('#event-gallery'),
		modalNoGalleryFallback = eventDetailModal.find('#no-gallery-fallback'),
		modalGalleryContent = modalGalleryHolder.find('#gallery-content'),
		modalCoverPhotoSection = eventDetailModal.find('#cover-photo-section'),
		modalCoverPhotoHolder = modalCoverPhotoSection.find('#event-cover-photo'),
		modalNoCoverPhotoFallback = modalCoverPhotoSection.find('#no-cover-photo-fallback'),
		modalGallerySelect = eventDetailModal.find('#select-gallery'),
		modalCoverPhotoSelect = eventDetailModal.find('#select-cover-photo'),
		coverPhotoData,
		galleryData;

	coverPhotoUrl = (typeof coverPhotoUrl === "undefined") ? 
					modalCoverPhotoHolder.attr('src') : coverPhotoUrl;
	
	if(coverPhotoUrl || coverPhotoTitle) {
		coverPhotoData = coverPhotoTitle;
	} else {
		coverPhotoData = '';
	}

	modalCoverPhotoSelect.attr('data-cover-photo', coverPhotoData); 

	if(gallery) {
		galleryData = gallery;
		unhideGallery();
		setupCoverPhoto();
	} else {
		galleryData = '';
		hideGallery();
	}
	
	modalGallerySelect.attr('data-gallery', galleryData);

	function unhideGallery() {
		unhide(modalGalleryHolder);
		unhide(modalCoverPhotoSection);
		hide(modalNoGalleryFallback);
		modalDescriptionSection.removeClass('col-xs-12');
		modalDescriptionSection.addClass('col-xs-6');
	}

	function hideGallery() {
		unhide(modalNoGalleryFallback);
		hide(modalGalleryHolder);
		hide(modalCoverPhotoSection);
		modalDescriptionSection.removeClass('col-xs-6');
		modalDescriptionSection.addClass('col-xs-12');
	}

	function setupCoverPhoto() {
		if(coverPhotoUrl) {
			modalCoverPhotoHolder.attr('src', coverPhotoUrl);
			unhide(modalCoverPhotoHolder);
			hide(modalNoCoverPhotoFallback);
		} else {
			unhide(modalNoCoverPhotoFallback);
			hide(modalCoverPhotoHolder);
		}
	}
}

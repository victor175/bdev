$('.home-link-inactive, .help-link-inactive, .signup-link-inactive, .login-link-inactive, .home-link, \
		.help-link, .signup-link, .login-link, .profile-link, .logout-link, .gallery-link')
	.click(function(){
	redirectToCorrespondingView(this);
});

$(window).hashchange(function(){
	onHashChange();
});

function onHashChange($rootScope) {
	$rootScope = (typeof $rootScope === "undefined") ? getRootScope() : $rootScope;

	var demandedRes = location.hash;

	if(demandedRes != $rootScope.lastHash){
		$rootScope.lastHash = demandedRes;

		var partialName = extractPartialNameFromDemandedRes(demandedRes),
			menuLink = convertPartialNameToMenuLink(partialName);

		activatePageLink(menuLink, $rootScope);
		updateMessagesList();
	}
}

$('#search-options ul li a').click(function(){
	$('#search-filters').text($(this).text());
	applySearchCriteria();
});

$('#language-options ul li a').click(function(){
	$('#language').text($(this).text());
	changePageLanguage($(this).attr('id'));
});

function initLangBar($rootScope) {
	$rootScope = (typeof $rootScope === "undefined") ? getRootScope() : $rootScope;

	var currentUrl = getRootUrl().split('/'),
		currLangPosInSplittedUrl = currentUrl.length - 2,
		currentLang = currentUrl[currLangPosInSplittedUrl],
		languageLabel = $rootScope.languageLabels[currentLang];

	$rootScope.currentLang = currentLang;
	$('#language').text(languageLabel);
}

function signalizeGalleryViewLoaded() {
		console.log('signalizing');
	var $rootScope = getRootScope(),
		currentImagesCount = getCurrentImagesCount(),
		galleryLoadingInterval = 
			setInterval(function() {	
				var newImagesCount = getCurrentImagesCount();

				console.log('waiting');

				if(currentImagesCount < newImagesCount) {
					clearInterval(galleryLoadingInterval);
					$rootScope.$broadcast('$viewContentLoaded');
				}
			}, 500);
}

var mainModule = 
	angular.module('EP',
	['ngRoute',
	'ngCookies',
	'ui.router',
	'ui.bootstrap']).run(function($rootScope, $templateCache) {
	$rootScope.messagesWidgetUrl = getPartialsRootUrl + 'messages_widget',
	$rootScope.eventObservationSubmissionUrl = getPartialsRootUrl + 'profile/observer',
	$rootScope.eventsViewUrl = getPartialsRootUrl + 'events_view',
	$rootScope.eventShortTemplateUrl = getPartialsRootUrl + 'event_short_template',
	$rootScope.messagesUrl = getRootUrl() + 'messages/',
	$rootScope.eventsDataUrl = getRootUrl() + 'get_all_events/',
	$rootScope.logoutUrl = getRootUrl() + 'logout/',
	$rootScope.lastActivated = '',
	$rootScope.activateClassString = 'active',
	$rootScope.lastHash = '',
	$rootScope.userType = '',
	$rootScope.userTypes = {
		observer: 'observer',
		organisator: 'organisator',
	},
	$rootScope.languages = {
		english: 'en',
		bulgarian: 'bg',
	},
	$rootScope.languageLabels = {
		en: 'English',
		bg: 'Български',
	},

	$rootScope.getEventsDataUrl = function (username) {
		return getRootUrl() + 'get_events/' + username + "/";
	},
	
	$rootScope.getEventRemovalUrl = function (eventTitle) {
		return getRootUrl() + 'remove_event/' + eventTitle + "/";
	},

	$rootScope.getEventShareUrl = function (title) {
		return getRootUrl() + '#/view/event/' + title;
	},

	$rootScope.getGalleriesUrl = function(username) {
		return getRootUrl() + 'get_galleries/' + username + "/";
	};

	$rootScope.$on('$routeChangeStart', function(event, next, current) {
		if (typeof(current) !== 'undefined'){
			$templateCache.remove(current.templateUrl);
		}
	});
	
	initLangBar($rootScope);

  /*window.onbeforeunload = confirmExit;
  
  function confirmExit() {
    return "You have attempted to leave this page.  If you have made any changes to the fields without clicking the Save button, your changes will be lost.  Are you sure you want to exit this page?";
  }
	*/

	onHashChange($rootScope);
});

mainModule
	.controller('MainController', MainController);
	
mainModule
	.controller('HelpController', HelpController);
	
mainModule
	.controller('LoginController', LoginController);
	
mainModule
	.controller('SignupController', SignupController);

mainModule
	.controller('ProfileController', ProfileController);

mainModule
	.controller('GalleriesController', GalleriesController);

mainModule
	.controller('ListMessages', ListMessages);

mainModule
	.controller('EventShortTemplateController', EventShortTemplateController);

mainModule
	.controller('EventsViewController', EventsViewController);

mainModule
	.directive('datetimeHolder', datetimeHolderDirective);

mainModule
	.directive('eventRemovalButton', eventRemovalButtonDirective);

mainModule
	.directive('eventOrganisatorButton', eventOrganisatorButtonDirective);

mainModule
	.directive('observeEventButton', observeEventButtonDirective);

mainModule
	.directive('shortEventHolder', shortEventHolderDirective);

mainModule
	.directive('gallerySelect', gallerySelectDirective);

mainModule
	.directive('photoSelect', photoSelectDirective);

mainModule
	.directive('editableDetail', editableDetailDirective);

mainModule
	.directive('nonEditableDetail', nonEditableDetailDirective);

mainModule
	.directive('organisatorProfileSpecific', organisatorProfileSpecificDirective);

mainModule
	.config(mainRoutes);

mainModule
	.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('{$');
	    $interpolateProvider.endSymbol('$}');
});

function mainRoutes($routeProvider){
	var partialsRootUrl = getPartialsRootUrl;
	$routeProvider
		.when('/', {
			templateUrl: partialsRootUrl + 'main',
			controller: 'MainController'
		})
		.when('/login', {
			templateUrl: partialsRootUrl + 'login',
			controller: 'LoginController'
		})
		.when('/signup', {
			templateUrl: partialsRootUrl + 'signup',
			controller: 'SignupController'
		})
		.when('/help', {
			templateUrl: partialsRootUrl + 'help',
			controller: 'HelpController'
		})
		.when('/profile/:type', {
			templateUrl: function(params) {
				var templateUrl = partialsRootUrl + 'profile/',
					method = 'GET',
					userType = getRootScope().userType;

				if(userType == '') {
					templateUrl += params.type;
					makeRequestWithAjax(templateUrl, method)
						.then(function (data) {
							if(data == '') {
								redirectToLocation('#/');
							}
						});
				} else {
					templateUrl += userType;
				}

				return templateUrl;
			},
			controller: 'ProfileController'
		})
		.when('/profile/', {
			resolve: {
				redirect: function ($location) {
					var userType = getRootScope().userType;
					
					if (userType == '') {
						$location.path('#/');
					} else {
						$location.path('/profile/' + userType);
					}
				}
			}
		})
		.when('/logout/', {
			resolve: {
				redirect: function () {
					logout();
				}
			}
		})
		.when('/view/event/:title', {
			resolve: {
				redirect: function ($location, $route) {
					var title = $route.current.params.title;
					$location.path('/');
			
					getRootScope().$on('$eventsLoaded', function(){
						moveEventToFirstPos(title)
							.then(function () {
								openEventDetails(title);
							})
							.fail(function (err) {
								console.log(err);
							});
					});
				}
			}
		})
		.when('/gallery/:year/:month/:day/:slug', {
			templateUrl: function(params) {
				var templateUrl = getRootUrl() + 'photologue/' + 'gallery/' + params.year +
					'/' + params.month + '/' + params.day + '/' + params.slug + "/";
						
				return templateUrl;
			},
			controller: 'GalleriesController'
		})
		.when('/gallery/:year/:month/:day', {
			templateUrl: function(params) {
				var templateUrl = getRootUrl() + 'photologue/' + 'gallery/' + params.year +
					'/' + params.month + '/' + params.day + "/";
						
				return templateUrl;
			},
			controller: 'GalleriesController'
		})
		.when('/gallery/:year/:month', {
			templateUrl: function(params) {
				var templateUrl = getRootUrl() + 'photologue/' + 'gallery/' + params.year +
					'/' + params.month + "/";
						
				return templateUrl;
			},
			controller: 'GalleriesController'
		})
		.when('/gallery/:slug', {
			templateUrl: function(params) {
				var templateUrl = getRootUrl() + 'photologue/' + 'gallery/' + params.slug + "/"
						
				return templateUrl;
			},
			controller: 'GalleriesController'
		})
		.when('/gallery', {
			templateUrl: getRootUrl() + 'photologue/' + 'gallery' + "/",
			controller: 'GalleriesController'
		})
		.when('/gallerylist', {
			templateUrl: getRootUrl() + 'photologue/' + 'gallerylist' + "/",
			controller: 'GalleriesController'
		})
		.when('/photo/:year/:month/:day/:slug', {
			templateUrl: function(params) {
				var templateUrl = getRootUrl() + 'photologue/' + 'photo/' + params.year +
					'/' + params.month + '/' + params.day + '/' + params.slug + "/";
						
				return templateUrl;
			},
			controller: 'GalleriesController'
		})
		.when('/photo/:year/:month/:day', {
			templateUrl: function(params) {
				var templateUrl = getRootUrl() + 'photologue/' + 'photo/' + params.year +
					'/' + params.month + '/' + params.day + "/";
						
				return templateUrl;
			},
			controller: 'GalleriesController'
		})
		.when('/photo/:year/:month', {
			templateUrl: function(params) {
				var templateUrl = getRootUrl() + 'photologue/' + 'photo/' + params.year +
					'/' + params.month + "/";
						
				return templateUrl;
			},
			controller: 'GalleriesController'
		})
		.when('/photo/:year', {
			templateUrl: function(params) {
				var templateUrl = getRootUrl() + 'photologue/' + 'photo/' + params.year + "/";
						
				return templateUrl;
			},
			controller: 'GalleriesController'
		})
		.when('/photo/:slug', {
			templateUrl: function(params) {
				var templateUrl = getRootUrl() + 'photologue/' + 'photo/' + params.slug + "/";
						
				return templateUrl;
			},
			controller: 'GalleriesController'
		})
		.when('/photo', {
			templateUrl: getRootUrl() + 'photologue/' + 'photo/',
			controller: 'GalleriesController'
		})
		.when('/photolist', {
			templateUrl: getRootUrl() + 'photologue/' + 'photolist/',
			controller: 'GalleriesController'
		})
		.otherwise({ redirectTo: '/' });
}

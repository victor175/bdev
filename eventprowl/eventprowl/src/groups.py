# coding: utf-8

from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _

TYPE_CHOICES =[('organisator', 'Organisator'),
					('observer', 'Observer')]

TYPE_CHOICES__FOR_UI =[('organisator', _('Organisator')),
					('observer', _('Observer'))]

ORGANISATORS_GROUP_NAME = 'Organisators'
OBSERVERS_GROUP_NAME = 'Observers'

def get_group(name):
	group, is_new = Group.objects.get_or_create(name=name)

	if is_new:
		add_group_permissions(group)

	return group

def add_group_permissions(group):
	if group.name == ORGANISATORS_GROUP_NAME:
		return 0
	elif group.name == OBSERVERS_GROUP_NAME:
		return 0

def add_user_to_appropriate_group(user, type):
	appropriate_group_name = find_appropriate_group(type)
	appropriate_group = get_group(appropriate_group_name)
	appropriate_group.user_set.add(user)

def find_appropriate_group(type):
	if type == TYPE_CHOICES[0][0]:
		appropriate_group_name = ORGANISATORS_GROUP_NAME
	else:
		appropriate_group_name = OBSERVERS_GROUP_NAME

	return appropriate_group_name

def find_appropriate_type(group):
	if group == ORGANISATORS_GROUP_NAME:
		appropriate_type_name = TYPE_CHOICES[0][0]
	elif group == OBSERVERS_GROUP_NAME:
		appropriate_type_name = TYPE_CHOICES[1][0]
	else:
		appropriate_group_name = None

	return appropriate_type_name

def find_users_in_group(group_name):
	group = Group.objects.filter(name=group_name).first()

	if not group:
		users = None
	else:
		users = group.user_set.all()
	
	return users

def user_has_group(user, group_name):
	has_group = False
	for group in user.groups.all():
		if group_name == group.name:
			has_group = True
			break

	return has_group

def is_organisator(user):
	return user_has_group(user, ORGANISATORS_GROUP_NAME)

def is_observer(user):
	return user_has_group(user, OBSERVERS_GROUP_NAME)

#content_type = ContentType.objects.get(app_label='eventprowl', model='BlogPost')
#permission = Permission.objects.create(codename='can_publish',
#                                       name='Can Publish Posts',
#                                       content_type=content_type)
#user = User.objects.get(username='duke_nukem')
#group = Group.objects.get(name='wizard')
#group.permissions.add(permission)
#user.groups.add(group)

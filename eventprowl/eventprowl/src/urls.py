# coding: utf-8

from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.views.generic import CreateView
from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns

""" Custom files """
from responses import get_events, get_all_events, remove_event, \
	get_user_messages, create_event, create_gallery, create_photo, \
	remove_photo, remove_gallery, get_galleries, update_event_details, \
	update_user
	
from views import main, login, signup, help, galleries, events_view, \
	messages_widget, event_short_template, observer_profile, \
	organisator_profile, main_layout, logout

processed_partial_patterns = patterns('',
	url(r'^main$', main, name='main'),
	url(r'^login$', login, name='login'),
	url(r'^signup$', signup, name='signup'),
	url(r'^help$', help, name='help'),
	url(r'^galleries$', galleries, name='galleries'),
	url(r'^events_view$', events_view, name='events_view'),
	url(r'^messages_widget$', messages_widget, name='messages_widget'),
	url(r'^event_short_template$', event_short_template, name='event_short_template'),
	url(r'^profile/observer$', observer_profile, name='observer_profile'),
	url(r'^profile/organisator$', organisator_profile, name='organisator_profile'),
)

urlpatterns = i18n_patterns('',
	url(r'^$', main_layout, name='main_layout'),
	url(r'^messages/$', get_user_messages, name='user_messages'),
	url(r'^logout/$', logout, name='logout'),
	url(r'^processed_partials/', include(processed_partial_patterns, namespace='processed_partials')),
	url(r'^get_events/(?P<username>\w{1,50})/', get_events, name='get_events'),
	url(r'^get_all_events/', get_all_events, name='get_all_events'),
	url(r'^get_galleries/(?P<username>\w{1,50})/', get_galleries, name='get_galleries'),
	url(r'^remove_event/(?P<title>.*)/', remove_event, name='remove_event'),
	url(r'^remove_photo/(?P<id>\w{1,50})/', remove_photo, name='remove_photo'),
	url(r'^remove_gallery/(?P<id>\w{1,50})/', remove_gallery, name='remove_gallery'),
	url(r'^create_event/', create_event, name='create_event'),
	url(r'^create_gallery/', create_gallery, name='create_gallery'),
	url(r'^create_photo/', create_photo, name='create_photo'),
	url(r'^update_event_details/', update_event_details, name='update_event_details'),
	url(r'^update_user/', update_user, name='update_user'),
	(r'^photologue/', include('photologue_custom.urls')),
	url(r'^photologue/', include('photologue.urls', namespace='photologue')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = "eventprowl.src.views.error404"

from django.utils.translation import ugettext as _
from django.forms.models import model_to_dict
from django.contrib.auth.models import User
from django.http import HttpResponse
from photologue.models import Photo, Gallery
import json as simplejson
import datetime
import json

""" Custom files """
from eventprowl.src.groups import TYPE_CHOICES
from eventprowl.src.forms import DATE_TIME_INPUT_FORMATS
from eventprowl.src.utils.model_utils import get_events_from_db

JSONIFIABLE_OBJECTS = {
	'event': 'event',
	'gallery': 'gallery',
	'photo': 'photo',
	'user': 'user',
}

NOT_FOUND_MESSAGES = {
	'user': 'No such user.',
	'gallery': 'No such gallery.',
}
 
class JsonResponse(HttpResponse):
	def __init__(self, data):
		content = simplejson.dumps(
			data,
			indent=2,
			ensure_ascii=False,
		)
		super(JsonResponse, self).__init__(content=content, content_type='application/json; charset=utf8')

def process_object_field_json(key, value, type):
	if isinstance(value, datetime.datetime):
		value = value.strftime(DATE_TIME_INPUT_FORMATS[0])

	if type == JSONIFIABLE_OBJECTS['event']:
		value = process_event_field_json(key, value)
	elif type == JSONIFIABLE_OBJECTS['gallery']:
		value =process_gallery_field_json(key, value)
	elif type == JSONIFIABLE_OBJECTS['photo']:
		value = process_photo_field_json(key, value)
	elif type == JSONIFIABLE_OBJECTS['user']:
		value = process_user_field_json(key, value)

	return value

def process_event_field_json(key, value):
	UNNEEDED_FIELDS = ['id']

	if key == TYPE_CHOICES[0][0]:
		user = User.objects.filter(id=value).first()
		value = user.username
	elif key == 'observers':
		value = len(value)
	elif key == 'cover_photo':
		if value:
			value = Photo.objects.filter(id=value).first()
			value = {
				'thumbnail_url': value.get_thumbnail_url(),
				'title': value.title,
			}
		else:
			value = False
	elif key == 'gallery':
		if value:
			value = Gallery.objects.filter(id=value).first()
			value = value.title
		else:
			value = False
	elif key == 'start_date':
		value = _(value)
	elif key == 'end_date':
		if not value:
			value = False
		else:
			value = _(value)
	elif key in UNNEEDED_FIELDS:
		value = -1

	return value

def process_user_field_json(key, value):
	UNNEEDED_FIELDS = ['id']

	if key in UNNEEDED_FIELDS:
		value = -1

	return value

def process_gallery_field_json(key, value):
	UNNEEDED_FIELDS = ['id', 'sites']

	if key in UNNEEDED_FIELDS:
		value = -1
	elif key == 'photos':
		value = get_photos_as_json(value)

	return value

def process_photo_field_json(key, value):
	UNNEEDED_FIELDS = ['id', 'sites', 'image', 'effect', 'crop_from', 'tags']

	if key in UNNEEDED_FIELDS:
		value = -1

	return value

def get_object_as_json(object, type):
	object_as_dict = model_to_dict(object)
	data = {}
	
	for key, value in object_as_dict.items():
		value = process_object_field_json(key, value, type)

		if value is not -1:
			data[key] = value

	if type in [JSONIFIABLE_OBJECTS['gallery'], JSONIFIABLE_OBJECTS['photo']]:
		add_obj_url_to_json(object, data)

	return data

def get_objects_as_json(objects, type):
	data = []

	for obj in objects:
		obj_json = get_object_as_json(obj, type)

		data.append(obj_json)

	return data

def add_obj_url_to_json(obj, json):
	url = obj.get_absolute_url()
	url_field = {
		'url': url,
	}

	json.update(url_field)

	if isinstance(obj, Photo):
		thumb_url = obj.get_thumbnail_url()
		thumb_url_field = {
			'thumbnail_url': thumb_url,
		}

		absolute_url = obj.image.url
		absolute_url_field = {
			'absolute_url': absolute_url,
		}

		display_url = obj.get_display_url()
		display_url_field = {
			'display_url': display_url,
		}

		json.update(thumb_url_field)
		json.update(absolute_url_field)
		json.update(display_url_field)

	return json

def get_events_for_user_as_json(user):
	if user:
		events = get_events_from_db(user)
		type = JSONIFIABLE_OBJECTS['event']
		events = get_objects_as_json(events, type)
	else:
		events = NOT_FOUND_MESSAGES['user']

	return events

def get_user_as_json(user):
	user_json = {}

	if user:
		type = JSONIFIABLE_OBJECTS['user']
		user_json = get_object_as_json(user, type)
	else:
		user_json = NOT_FOUND_MESSAGES['user']
		
	return user_json
	
def get_galleries_for_user_as_json(user):
	if user:
		galleries = user.custom.galleries.all()
		if galleries:
			type = JSONIFIABLE_OBJECTS['gallery']
			galleries = get_objects_as_json(galleries, type)
		else:
			galleries = []
	else:
		galleries = NOT_FOUND_MESSAGES['user']

	return galleries

def get_photos_as_json(photos):
	if photos:
		for key in range(0, len(photos)):
			photos[key] = Photo.objects.filter(id=photos[key]).first()

		type = JSONIFIABLE_OBJECTS['photo']
		photos = get_objects_as_json(photos, type)

	return photos

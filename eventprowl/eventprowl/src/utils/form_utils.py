from django.utils.safestring import mark_safe
from django.utils.html import format_html
from django.forms.utils import flatatt
from django.contrib import messages
from datetime import datetime
from random import randrange
from slugify import slugify
from django import forms

""" Custom files"""
from eventprowl.src.utils.collection_utils import delete_elem

DATE_TIME_INPUT_FORMATS = ['%Y/%m/%d %H:%M']

def form_has_errors(form):
	if not form.errors:
		has_errors = True
	else:
		has_errors = False

	return has_errors

def remove_form_errors(form, to_be_removed):
	if form_has_errors(form):
		errors = form.errors

	for error in to_be_removed:
		delete_elem(error, errors)

def show_form_errors(request, form):
	for field, errors in form.errors.items():
		for error in errors:
			messages.error(request, error)

def custom_slugify(string, model):
	slug = slugify(string)

	while (model.objects.filter(slug=slug).first() is not None):
		random_digit = randrange(1, 9)
		slug += str(random_digit)

	return slug

class DateTimeWidget(forms.DateTimeInput):
	def __init__(self, attrs=None):
		if attrs is not None:
			self.attrs = attrs.copy()
		else:
			self.attrs = {'class': 'datetimepicker'}

		if not 'placeholder' in self.attrs:
			date = datetime.now()
			date = date.strftime(DATE_TIME_INPUT_FORMATS[0])
			self.attrs['placeholder'] = date

		if not 'format' in self.attrs:
			self.attrs['format'] = DATE_TIME_INPUT_FORMATS[0]

	def render(self, name, value, attrs=None):
		if value is not None:
			if value != '':
				value = value.strftime(self.attrs['format'])
		return super(forms.DateTimeInput, self).render(name, value, attrs)

class SelectWithDefaultOptions(forms.Select):
	def render(self, name, value, attrs=None, choices=()):
		choices = ()
		choices += (('None', 'None'),)
		self.choices = choices

		if value is None:
				value = ''

		final_attrs = self.build_attrs(attrs, name=name)
		attributes = flatatt(final_attrs).encode('utf-8')
		output = [format_html('<select{0}>', attributes)]
		options = self.render_options((), [value])

		if options:
				output.append(options)
				output.append('</select>')
				
		return mark_safe('\n'.join(output))	

class CustomChoiceField(forms.ChoiceField):

	def validate(self, value):
		return True

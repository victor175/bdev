from photologue.models import Gallery, Photo

''' Custom files '''
from eventprowl.models import Event
from eventprowl.src.groups import is_organisator, is_observer

USER_REMOVABLE_OBJECTS = {
	'event': 'event',
	'photo': 'photo',
	'gallery': 'gallery',
}

def get_gallery(id):
	if id is not None:
		gallery = Gallery.objects.filter(id=id).first()
	else:
		gallery = None

	return gallery

def get_events_from_db(user):
	if is_organisator(user):
		events = Event.objects.filter(organisator=user).all()
	else:
		events = Event.objects.filter(observers__in=[user]).all()

	return events

def remove_event_for_user(user, event_title):
	if is_observer(user):
		remove_event_observation(user, event_title)
	else:
		remove_event(event_title)

def remove_event_observation(user, event_title):
	response = ''

	event = Event.objects.filter(title=event_title).first()
	fail_response = 'The event is not observed by this user.'
	response = alterObjectInM2MField(event.observers, user, response, fail_response, True)

	return response

def get_event(title):
	event = Event.objects.filter(title=title).first()
	
	return event

def get_photo(id):
	photo = Photo.objects.filter(id=id).first()

	return photo

def remove_event(title):
	event = get_event(title)
	if event:
		event.delete()

def remove_photo(id):
	get_photo(id).delete()

def remove_gallery(id):
	get_gallery(id).delete()

def remove_object(type, search_criteria, user = None):
	if type == USER_REMOVABLE_OBJECTS['photo']:
		remove_photo(search_criteria)
	elif type == USER_REMOVABLE_OBJECTS['event']:
		remove_event_for_user(user, search_criteria)
	elif type == USER_REMOVABLE_OBJECTS['gallery']:
		remove_gallery(search_criteria)

def alterObjectInM2MField(m2m_field, object, success_response, fail_response, shouldRemove):
	last_count = len(m2m_field.all())

	if shouldRemove:
		m2m_field.remove(object)
	else:
		m2m_field.add(object)

	count_after_addition = len(m2m_field.all())

	if(last_count == count_after_addition):
		response = fail_response
	else:
		response = success_response

	return response

from django.http import HttpResponse
from django.shortcuts import render

""" Custom files """
from eventprowl.src.forms import SignUpForm, LoginForm, EventCreationForm, \
	PasswdForm, GalleryCreationForm, EventDetailForm

def send_partial_if_authenticated(request, name):
	if request.user.is_authenticated():
		return send_partial(request, name)
	else:
		return HttpResponse('')

def send_partial(request, demanded_partial_name):
	request.META["CSRF_COOKIE_USED"] = True

	html_file_suffix = ".html"

	if demanded_partial_name == 'signup':
		form = SignUpForm()
		forms = {'form': form}
	elif demanded_partial_name == 'login':
		form = LoginForm()
		forms = {'form': form}
	elif demanded_partial_name == 'organisator_profile':
		event_creation_form = EventCreationForm()
		gallery_creation_form = GalleryCreationForm()

		forms = {'event_creation_form': event_creation_form,
					'gallery_creation_form': gallery_creation_form}
	elif demanded_partial_name == 'events_view':
		password_form = PasswdForm()
		event_detail_form = EventDetailForm()
		forms = {'password_form': password_form,
						'event_detail_form': event_detail_form}
	else:
		return render(request, demanded_partial_name + html_file_suffix)

	return render(request, demanded_partial_name + html_file_suffix, forms)

def delete_elem(elem, query_set):
    if elem in query_set:
        del query_set[elem]

def add_field_to_query_set(query_set, field, field_data):
    if field not in query_set:
        query_set[field] = field_data

def add_fields_to_query_set(query_set, fields, fields_data):
    for field in fields:
        add_field_to_query_set(query_set, field, fields_data[field])
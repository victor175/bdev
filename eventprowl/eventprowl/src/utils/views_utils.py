from django.contrib.auth import authenticate, login, logout
from django.utils.translation import ugettext_lazy as _
from django.contrib.messages import get_messages
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.contrib import messages

""" Custom files """
from eventprowl.src.groups import is_organisator, add_user_to_appropriate_group, find_appropriate_type
from eventprowl.models import Event
from eventprowl.src.forms import SignUpForm, EventCreationForm
from eventprowl.src.utils.form_utils import show_form_errors
from eventprowl.src.utils.model_utils import alterObjectInM2MField
from eventprowl.src.utils.json_utils import JsonResponse, get_user_as_json

MESSAGES = {
	'observation_failure': 'The event is already observed by this user.',
	'account_creation_success': _("Your account has been created successfully!"),
	'login_success': _("You logged in successfully!"),
	'disabled_account': _("The password you entered is valid, but the account has been disabled!"),
	'wrong_credentials': _("The username and/or password you entered were incorrect."),
	'logout_success': _("You logged out successfully!"),
}

def handle_profile_post(request):
	response = HttpResponse('')

	if request.POST.get('email', None) is not None:
		response = handle_email_change(request)
		return response

	if is_organisator(request.user):
		if request.POST.get('about', None) is not None:
			response = handle_about_change(request)
	else:
		if request.POST.get('observation_event_title', None) is not None:
			response = handle_event_observation(request)

	return response

def handle_event_observation(request):
	response = ''

	user = request.user
	eventTitle = request.POST.get('observation_event_title', None)
	event = Event.objects.all().filter(title=eventTitle).first()
	fail_response = MESSAGES['observation_failure'] 
	response = alterObjectInM2MField(event.observers, user, response, fail_response, False)

	return HttpResponse(response)

def handle_signup(request):
	redirection_url = ''
	form = SignUpForm(request.POST)

	if form.is_valid():
		user = form.save()
		type = request.POST['type']
		add_user_to_appropriate_group(user, type)
		messages.success(request, MESSAGES['account_creation_success'])
		redirection_url = '#/login'
	else:
		show_form_errors(request, form)

	return HttpResponse(redirection_url)

def handle_login(request):
	data = {}
	username = request.POST['username']
	password = request.POST['password']
	user = authenticate(username=username, password=password)

	if user is not None:
		if user.is_active:
			group = user.groups.all()[0].name
			type = find_appropriate_type(group)
			login(request, user)
			messages.success(request, MESSAGES['login_success'])
			data = get_user_as_json(user)
			data['redirection_url'] =  '#/profile/' + type
		else:
			messages.error(request, MESSAGES['disabled_account'])
	else:
		messages.error(request, MESSAGES['wrong_credentials'])

	return JsonResponse(data)

def handle_logout(request):
	response = '#/'
	logout(request)
	messages.success(request, MESSAGES['logout_success'])
	return HttpResponse(response)

def handle_about_change(request):
	redirection_url = ''

	about = request.POST['about']
	custom_user_fields = request.user.custom
	custom_user_fields.about = about
	custom_user_fields.save()

	return HttpResponse(redirection_url)   

def handle_email_change(request):
	redirection_url = ''

	email = request.POST['email']
	user = request.user
	user.email = email
	user.save()

	return HttpResponse(redirection_url)

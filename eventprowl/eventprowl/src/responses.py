# coding: utf-8

from django.contrib.auth.models import User
from eventprowl.models import Event
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.messages import get_messages

""" Custom files """
from eventprowl.src.forms import PasswdForm, GalleryCreationForm, EventCreationForm, \
	PhotoCreationForm, EventDetailForm, UserUpdateForm
from eventprowl.src.groups import ORGANISATORS_GROUP_NAME, find_users_in_group
from eventprowl.src.utils.json_utils import JsonResponse, get_events_for_user_as_json, \
	get_galleries_for_user_as_json
from eventprowl.src.utils.form_utils import show_form_errors
from eventprowl.src.utils.model_utils import remove_event_for_user, alterObjectInM2MField, \
	remove_photo, USER_REMOVABLE_OBJECTS, remove_object

def update_event_details(request):
	response = handle_event_details_update(request)

	return response

def handle_event_details_update(request):
	form = EventDetailForm(request.POST)
	redirection_url = ''

	if form.is_valid():
		form.save()
	else:
		show_form_errors(request, form)

	return HttpResponse(redirection_url)

def create_event(request):
	response = handle_event_creation(request)

	return response

def update_user(request):
	response = handle_user_update(request)

	return response

def handle_user_update(request):
	redirection_url = ''
	if request.method == 'POST':
		form = UserUpdateForm(request.POST, request.user)

		if form.is_valid():
			form.save()
		else:
			show_form_errors(request, form)

	return HttpResponse(redirection_url)

def handle_event_creation(request):
	form = EventCreationForm(request.POST)
	redirection_url = ''

	if form.is_valid():
		event = form.save(commit=False)
		event.organisator = request.user
		event.save()
	else:
		show_form_errors(request, form)

	return HttpResponse(redirection_url)

def create_gallery(request):
	response = handle_gallery_creation(request)

	return response

def create_photo(request):
	response = handle_photo_creation(request)

	return response

def handle_photo_creation(request):
	redirection_url = ''
	form = PhotoCreationForm(request.POST, request.FILES)

	if form.is_valid():
		form.save()
	else:
		show_form_errors(request, form)

	return HttpResponse(redirection_url)

def handle_gallery_creation(request):
	form = GalleryCreationForm(request.POST, request.user)
	redirection_url = ''

	if form.is_valid():
		form.save()
	else:
		show_form_errors(request, form)

	return HttpResponse(redirection_url)

def get_user_messages(request):
	storage = get_messages(request)
	data = []

	for message in storage:
		data.append({
			'text' : message.message,
			'status' : message.tags.replace('error', 'danger'),
		})

	return JsonResponse(data)

def get_events(request, username):
	user = User.objects.filter(username=username).first()
	data = get_events_for_user_as_json(user)
	
	return JsonResponse(data)

def remove_event(request, title):
	type = USER_REMOVABLE_OBJECTS['event']
	
	return remove_object_securely(request, type, title)

def remove_photo(request, id):
	type = USER_REMOVABLE_OBJECTS['photo']
	
	return remove_object_securely(request, type, id)

def remove_gallery(request, id):
	type = USER_REMOVABLE_OBJECTS['gallery']
	
	return remove_object_securely(request, type, id)

def get_galleries(request, username):
	user = User.objects.filter(username=username).first()
	galleries = get_galleries_for_user_as_json(user)

	return JsonResponse(galleries)

def remove_object_securely(request, type, search_criteria):
	redirection_url = ''

	if request.method == 'POST':
		user = request.user
		form = PasswdForm(request.POST, user)

		if form.is_valid():
			remove_object(type, search_criteria, user)
		else:
			show_form_errors(request, form)

	return  HttpResponse(redirection_url)

def get_all_events(request):
	organisators = find_users_in_group(ORGANISATORS_GROUP_NAME)
	data = []
	if not organisators:
		data = 'No organisators found'
	else:
		for user in organisators:
			user_events = get_events_for_user_as_json(user)
		
			for event in user_events:
				data.append(event)
	
	return JsonResponse(data)

# coding: utf-8

from djangular.forms import NgFormValidationMixin, NgModelForm
from django.contrib.auth.hashers import check_password
from django.utils.translation import ugettext_lazy as _
from photologue.models import Gallery, Photo
from django.contrib.auth.models import User
from django import forms

""" Custom files """
from eventprowl.src.utils.model_utils import alterObjectInM2MField, get_gallery
from eventprowl.src.utils.form_utils import custom_slugify, DateTimeWidget, \
	SelectWithDefaultOptions, DATE_TIME_INPUT_FORMATS, CustomChoiceField
from eventprowl.models import Event, CustomFields, EVENT_TITLE_MAX_LENGTH, \
	USER_ABOUT_MAX_LENGTH
from eventprowl.src.groups import TYPE_CHOICES, TYPE_CHOICES__FOR_UI

ERROR_MESSAGES = {
	'duplicate_username': _("The username is already taken, please try another one."),
	'duplicate_email': _("The email is already taken, please try another one."),
	'wrong_password': _("This password is invalid."),
	'event_title_exceeded': _("The title you entered is too long."),
	'user_about_exceeded': _("The desciption you entered in too long"),
}

class SignUpForm(NgFormValidationMixin, NgModelForm):
	def __init__(self, *args, **kwargs):
		super(SignUpForm, self).__init__(*args, **kwargs)
		self.fields['email'].required = True

	radio_widget = forms.RadioSelect(attrs={
		'class': 'inline-radio',
		'name': 'type',
		'id': 'signup-form-field4',
	})

	type = forms.ChoiceField(choices=TYPE_CHOICES__FOR_UI, required=True, widget=radio_widget)

	class Meta:
		model = User
		fields = ('username', 'email', 'password')
		widgets = {
			'username': forms.TextInput(attrs={
				'class': 'form-control ',
				'name': 'username',
				'id': 'signup-form-field1',
				'placeholder': _('username')
			}),
			'email': forms.EmailInput(attrs={
				'class': 'form-control ',
				'name': 'email',
				'id': 'signup-form-field2',
				'placeholder': 'somebody@example.com'
			}),
			'password': forms.PasswordInput(attrs={
				'class': 'form-control ',
				'name': 'key',
				'id': 'signup-form-field3',
				'placeholder': _('password')
			})
		}

	def clean_username(self):
		username = self.cleaned_data["username"]
		user = User.objects.filter(username=username).first()
		if not user:
			return username

		raise forms.ValidationError(
			ERROR_MESSAGES['duplicate_username'],
			code='duplicate_username',
		)

	def clean_email(self):
		email = self.cleaned_data["email"]
		user = User.objects.filter(email=email).first()
		if not user:
			return email

		raise forms.ValidationError(
			ERROR_MESSAGES['duplicate_email'],
			code='duplicate_email',
		)

	def save(self, commit=True):
		user = super(SignUpForm, self).save(commit=False)
		user.set_password(self.cleaned_data["password"])
		if commit:
			user.save()
			CustomFields.objects.create(about='', user=user)
			
		return user

class LoginForm(NgFormValidationMixin, NgModelForm):
	class Meta:
		model = User
		fields = ('username', 'password')
		widgets = {
			'username': forms.TextInput(attrs={
				'class': 'form-control ',
				'name': 'username',
				'id': 'login-form-field1',
				'placeholder': _('username')
			}),
			'password': forms.PasswordInput(attrs={
				'class': 'form-control ',
				'name': 'key',
				'id': 'login-form-field2',
				'placeholder': _('password')
			}),
		}

class EventCreationForm(NgFormValidationMixin, NgModelForm):
	def __init__(self, *args, **kwargs):
		super(EventCreationForm, self).__init__(*args, **kwargs)
		self.fields['title'].required = True
		self.fields['description'].required = False
		self.fields['start_date'].required = True
		self.fields['start_date'].input_formats = DATE_TIME_INPUT_FORMATS
		self.fields['end_date'].required = False
		self.fields['end_date'].input_formats = DATE_TIME_INPUT_FORMATS

	class Meta:
		model = Event
		fields = ('title', 'description', 'start_date', 'end_date')
		widgets = {
			'title': forms.TextInput(attrs={
				'class': 'form-control ',
				'name': 'title',
				'id': 'event-creation-form-field1',
			}),
			'description': forms.Textarea(attrs={
				'class': 'form-control ',
				'name': 'desc',
				'id': 'event-creation-form-field2',
				'rows': '3'
			}),
			'start_date': DateTimeWidget(attrs = {
				'class': 'form-control datetimepicker',
				'id': 'event-creation-form-field3'
			}),
			'end_date': DateTimeWidget(attrs = {
				'class': 'form-control datetimepicker',
				'id': 'event-creation-form-field4'
			}),
		}

class GalleryCreationForm(NgFormValidationMixin, NgModelForm):
	def __init__(self, *args, **kwargs):
		super(GalleryCreationForm, self).__init__(*args, **kwargs)
		self.fields['title'].required = True

		arguments_length = len(args)
		if arguments_length > 0:
			self.user = args[arguments_length - 1]

	class Meta:
		model = Gallery
		fields = ('title',)
		widgets = {
			'title': forms.TextInput(attrs={
				'class': 'form-control ',
				'name': 'title',
				'id': 'gallery-creation-form-field1',
			}),
		}

	def save(self, commit=True):
		gallery = super(GalleryCreationForm, self).save(commit=False)
		title = self.cleaned_data["title"]
		gallery.slug = custom_slugify(title, Gallery)

		if commit:
			gallery.save()
			alterObjectInM2MField(self.user.custom.galleries, gallery, '', '', False)
			
		return gallery


class PhotoCreationForm(NgFormValidationMixin, NgModelForm):
	def __init__(self, *args, **kwargs):
		super(PhotoCreationForm, self).__init__(*args, **kwargs)
		self.fields['title'].required = True
		self.fields['image'].required = True

		arguments_length = len(args)
		if arguments_length > 0:
			post_data = args[0]
			self.gallery_id = post_data.get('gallery_id', None)

	class Meta:
		model = Photo
		fields = ('title', 'image',)
		widgets = {
			'title': forms.TextInput(attrs={
				'class': 'form-control ',
				'name': 'title',
				'id': 'photo-creation-form-field1',
			}),
			'image': forms.FileInput(attrs={
				'class': 'form-control ',
				'name': 'photo-upload',
				'id': 'photo-creation-form-field2',
			}),
		}

	def save(self, commit=True):
		photo = super(PhotoCreationForm, self).save(commit=False)
		title = self.cleaned_data["title"]
		photo.slug = custom_slugify(title, Photo)

		if commit:
			photo.save()
			gallery = get_gallery(self.gallery_id)
			alterObjectInM2MField(gallery.photos, photo, '', '', False)
			
		return photo

class PasswdForm(forms.Form):
	def __init__(self, *args, **kwargs):
		super(PasswdForm, self).__init__(*args, **kwargs)
		
		arguments_length = len(args)
		if arguments_length > 0:
			self.user = args[arguments_length - 1]

		self.fields['password'].required = True
	
	password_widget = forms.PasswordInput(attrs={
		'class': 'form-control ',
		'name': 'password',
		'id': 'password',
	})

	password = forms.CharField(required=True, widget=password_widget)

	def clean(self):
		cleaned_data = super(PasswdForm, self).clean()

		if not check_password(cleaned_data['password'], self.user.password):
			raise forms.ValidationError(
				ERROR_MESSAGES['wrong_password'],
				code='wrong_password',
			)
			return False

		return True

def validate_event_title(value):
	if(len(value) > EVENT_TITLE_MAX_LENGTH):
		raise ValidationError(ERROR_MESSAGES['event_title_exceeded'])
	
	return True

class EventDetailForm(NgFormValidationMixin, NgModelForm):
	def __init__(self, *args, **kwargs):
		super(EventDetailForm, self).__init__(*args, **kwargs)
		self.fields['end_date'].required = False
		self.fields['description'].required = False
		self.fields['start_date'].input_formats = DATE_TIME_INPUT_FORMATS
		self.fields['end_date'].input_formats = DATE_TIME_INPUT_FORMATS
		self.fields['gallery'].required = False
		self.fields['cover_photo'].required = False

		arguments_length = len(args)
		if arguments_length > 0:
			post_data = args[0]
			self.old_title = post_data.get('old_title', None)
			self.gallery_title = post_data.get('gallery_title', None)

	cover_photo_widget = SelectWithDefaultOptions(attrs={
			'class': 'chosen-select-no-single',
			'id': 'select-cover-photo',
			'data-placeholder': 'Select Cover Photo',
			'tabindex': '-1',
		})

	gallery_widget = SelectWithDefaultOptions(attrs={
			'class': 'chosen-select-no-single',
			'id': 'select-gallery',
			'data-placeholder': 'Select Gallery',
			'gallery-select': '',
			'tabindex': '-1',
			'organisator-profile-specific': '',
		})
	
	title_widget = forms.TextInput(attrs={
			'id': 'event-editable-title',
			'editable-detail': '',
		})
	
	gallery = CustomChoiceField(widget = gallery_widget)
	cover_photo = CustomChoiceField(widget = cover_photo_widget)
	title = forms.CharField(widget = title_widget, validators = [validate_event_title])

	class Meta:
		model = Event
		fields = ('description', 'end_date', 'start_date')
		widgets = {
			'description': forms.TextInput(attrs={
				'class': 'panel-body',
				'id': 'event-description-editable',
				'editable-detail': '',
			}),
			'start_date': DateTimeWidget(attrs = {
				'class': 'datetimepicker col-xs-6',
				'id': 'event-start-date-editable',
				'editable-detail': '',
			}),
			'end_date': DateTimeWidget(attrs = {
				'class': 'datetimepicker col-xs-6',
				'id': 'event-end-date-editable',
				'editable-detail': '',
			}),
		}

	def save(self, commit=True):
		event = Event.objects.filter(title=self.old_title).first()

		event.title = self.cleaned_data['title']
		event.description = self.cleaned_data['description']
		event.start_date = self.cleaned_data['start_date']
		event.end_date = self.cleaned_data['end_date']
		print event.end_date

		cover_photo_title = self.cleaned_data['cover_photo']
		cover_photo = Photo.objects.filter(title=cover_photo_title).first()
		
		gallery = Gallery.objects.filter(title=self.gallery_title).first()
	
		event.cover_photo = cover_photo
		event.gallery = gallery

		if commit:
			event.save()
			
		return event

class UserUpdateForm(NgFormValidationMixin, NgModelForm):
	def __init__(self, *args, **kwargs):
		super(UserUpdateForm, self).__init__(*args, **kwargs)
		self.fields['email'].required = True
		self.fields['username'].required = True

		arguments_length = len(args)
		if arguments_length > 0:
			self.user = args[arguments_length - 1]

	about_widget = forms.TextInput(attrs={
			'editable-detail': '',
		})
	
	about = forms.CharField(widget = about_widget);

	class Meta:
		model = User
		fields = ('username', 'email')
		widgets = {
			'username': forms.TextInput(attrs={
				'class': 'form-control ',
				'name': 'username',
				'id': 'signup-form-field1',
				'placeholder': _('username'),
				'editable-detail': '',
			}),
			'email': forms.EmailInput(attrs={
				'class': 'form-control ',
				'name': 'email',
				'id': 'signup-form-field2',
				'placeholder': 'somebody@example.com',
				'editable-detail': '',
			}),
		}

	def clean_username(self):
		username = self.cleaned_data["username"]
		user = User.objects.filter(username=username).first()
		if not user or user == self.user:
			return username

		raise forms.ValidationError(
			ERROR_MESSAGES['duplicate_username'],
			code='duplicate_username',
		)

	def clean_email(self):
		email = self.cleaned_data["email"]
		user = User.objects.filter(email=email).first()
		if not user or user == self.user:
			return email

		raise forms.ValidationError(
			ERROR_MESSAGES['duplicate_email'],
			code='duplicate_email',
		)
	
	def clean_about(self):
		about = self.cleaned_data['about']
		if(about.length < USER_ABOUT_MAX_LENGTH):
			return about	
			
		raise forms.ValidationError(
			ERROR_MESSAGES['user_about_exceeded'],
			code='user_about_exceeded',
		)

	def save(self, commit=True):
		user = super(UserUpdateForm, self).save(commit=False)
		print user.customs.about

		if commit:
			user.save()
			
		return user

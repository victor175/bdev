# coding: utf-8

from django.shortcuts import render
from django.http import HttpResponseNotFound
import inspect

""" Custom files """
from eventprowl.src.utils.views_utils import handle_signup, handle_login, handle_logout, handle_profile_post
from eventprowl.src.utils.partial_utils import send_partial, send_partial_if_authenticated

def signup(request):
	response = ''
	if request.method == 'GET':
		response = send_partial(request, inspect.stack()[0][3])
	else:
		response = handle_signup(request)

	return response

def login(request):
	response = ''
	if request.method == 'GET':
		response = send_partial(request, inspect.stack()[0][3])
	else:
		response = handle_login(request)

	return response

def logout(request):
	response = ''
	if request.method == 'POST':
		response = handle_logout(request)

	return response
	
def help(request):
	response = ''
	if request.method == 'GET':
		response = send_partial(request, inspect.stack()[0][3])

	return response

def main(request):
	response = ''
	if request.method == 'GET':
		response = send_partial(request, inspect.stack()[0][3])

	return response

def observer_profile(request):
	response = ''
	if request.method == 'GET':
		response = send_partial_if_authenticated(request, inspect.stack()[0][3])
	else:
		response = handle_profile_post(request)

	return response

def organisator_profile(request):
	response = ''
	if request.method == 'GET':
		response = send_partial_if_authenticated(request, inspect.stack()[0][3])
	else:
		response = handle_profile_post(request)

	return response

def galleries(request):
	response = ''
	if request.method == 'GET':
		response = send_partial_if_authenticated(request, inspect.stack()[0][3])
	else:
		response = ''

	return response

def events_view(request):
	response = ''
	if request.method == 'GET':
		response = send_partial(request, inspect.stack()[0][3])

	return response

def event_short_template(request):
	response = ''
	if request.method == 'GET':
		response = send_partial(request, inspect.stack()[0][3])

	return response

def messages_widget(request):
	response = ''
	if request.method == 'GET':
		response = send_partial(request, inspect.stack()[0][3])

	return response

def main_layout(request):
	response = ''
	if request.method == 'GET':
		response = render(request, 'main_layout.html')

	return response

def error404(request):
	return HttpResponseNotFound(render_to_string('404.html'))

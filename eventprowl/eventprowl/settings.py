# coding: utf-8

"""
Django settings for eventprowl project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'b%c*9!vpd7u)o$y6g(%*d_s7$f9nw2_9+eq&ano=xk1pp40bcg'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
DEPLOY = False

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = (
	'django.contrib.admin',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	'django.contrib.sites',
	'eventprowl',
	'photologue_custom',
	'compressor',
	'simplejson',
	'djangular',
	'photologue',
	'sortedm2m',	
)

MIDDLEWARE_CLASSES = (
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.common.CommonMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	'django.middleware.clickjacking.XFrameOptionsMiddleware',
	'django.middleware.gzip.GZipMiddleware',
	'django.middleware.locale.LocaleMiddleware',
	'eventprowl.src.middleware.ExceptionLoggingMiddleware'
)

ROOT_URLCONF = 'eventprowl.src.urls'

WSGI_APPLICATION = 'eventprowl.wsgi.application'

# ------- import DB url_config --------
# # Parse database configuration from $DATABASE_URL
import dj_database_url

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

if not DEPLOY:
	DATABASES = {
		'default': {
			'ENGINE': 'django.db.backends.postgresql_psycopg2', 
			'NAME': 'eventprowldb',
			'USER': 'vicky',                      
			'PASSWORD': '',             
			'HOST': 'localhost',                     
			'PORT': '',                     
		}
	}
else:
	DATABASES = {'default': dj_database_url.config(default=os.environ["HEROKU_POSTGRESQL_AMBER_URL"])}

#DATABASES = {'default': dj_database_url.config(default='postgres://localhost')}


# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# ------ My stuff -------
from django.utils.translation import ugettext_lazy as _

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = ['*']

# Static asset configuration
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

STATIC_ROOT = 'staticfiles'
STATIC_URL = '/static/'

MEDIA_ROOT = 'mediafiles'
MEDIA_URL = '/media/'

COMPRESS_ROOT = 'staticfiles'

SITE_ID = 1

STATICFILES_DIRS = (
	os.path.join(BASE_DIR, 'static'), 
)

LOCALE_PATHS = (
	os.path.join(BASE_DIR, 'locale'),
)

TEMPLATE_DIRS = (
	# Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
	# Always use forward slashes, even on Windows.
	# Don't forget to use absolute paths, not relative paths.
	os.path.join(PROJECT_ROOT, 'templates').replace('\\','/'),
)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
	'django.template.loaders.filesystem.Loader',
	'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

STATICFILES_FINDERS = (
	'django.contrib.staticfiles.finders.FileSystemFinder',
	'django.contrib.staticfiles.finders.AppDirectoriesFinder',
	'compressor.finders.CompressorFinder',
)

COMPRESS_PRECOMPILERS = (
		('text/x-scss', 'django_libsass.SassCompiler'),
)

TEMPLATE_CONTEXT_PROCESSORS = (
	'django.core.context_processors.request',
)

TEMPLATES = {
	'django.template.context_processors.i18n',
}

LANGUAGES = (
	('en', _('English')),
	('bg', _('Bulgarian')),
)

from django.http import HttpResponse
from django.core.cache import cache
from django.shortcuts import render
import inspect

from photologue.views import PhotoListView, PhotoDetailView, GalleryListView, \
	GalleryDetailView, PhotoArchiveIndexView, PhotoDateDetailView, PhotoDayArchiveView, \
	PhotoYearArchiveView, PhotoMonthArchiveView, GalleryArchiveIndexView, GalleryYearArchiveView, \
	GalleryDateDetailView, GalleryDayArchiveView, GalleryMonthArchiveView

''' Custom files'''
from eventprowl.src.forms import PhotoCreationForm, PasswdForm

def gallery_date_detail(request, year, month, day, slug):
	view = GalleryDateDetailView.as_view(month_format='%m')(request, year=year, month=month, day=day, slug=slug)
	return view

def gallery_day_archive(request, year, month, day):
	view = GalleryDayArchiveView.as_view(month_format='%m')(request, year=year, month=month, day=day)
	return view

def gallery_month_archive(request, year, month):
	view = GalleryMonthArchiveView.as_view(month_format='%m')(request, year=year, month=month)
	return view

def gallery_year_archive(request, year):
	view = GalleryYearArchiveView.as_view()(request, year=year)
	return view

def gallery(request):
	view = GalleryArchiveIndexView.as_view()(request)
	return view

def gallery_detail(request, slug):
	view = GalleryDetailView.as_view()(request, slug=slug)
	photo_creation_form = PhotoCreationForm()
	gallery_removal_form = PasswdForm()
	view.context_data.update({
		'photo_creation_form': photo_creation_form,
		'gallery_removal_form': gallery_removal_form
	});

	return view

def gallerylist(request):
	view = GalleryListView.as_view()(request)
	return view

def photo_date_detail(request, year, month, day, slug):
	view = PhotoDateDetailView.as_view(month_format='%m')(request, year=year, month=month, day=day, slug=slug)
	return view

def photo_day_archive(request, year, month, day):
	view = PhotoDayArchiveView.as_view(month_format='%m')(request, year=year, month=month, day=day)
	return view

def photo_month_archive(request, year, month):
	view = PhotoMonthArchiveView.as_view(month_format='%m')(request, year=year, month=month)
	return view

def photo_year_archive(request, year):
	view = PhotoYearArchiveView.as_view()(request, year=year)
	return view

def photo(request):
	view = PhotoArchiveIndexView.as_view()(request)
	return view

def photo_detail(request, slug):
	view = PhotoDetailView.as_view()(request, slug=slug)
	form = PasswdForm()
	view.context_data.update({'form': form});

	return view

def photolist(request):
	view = PhotoListView.as_view()(request)
	return view
from django.conf.urls import *
from django.views.generic import RedirectView
from django.core.urlresolvers import reverse_lazy

from photologue.views import GalleryListView, PhotoListView, GalleryDayArchiveOldView, GalleryDateDetailOldView, \
	PhotoDayArchiveOldView, GalleryMonthArchiveOldView, PhotoDateDetailOldView, \
	PhotoMonthArchiveOldView

''' Custom files'''
from photologue_custom.views import gallerylist, gallery_date_detail, gallery_day_archive, \
	gallery_month_archive, gallery_year_archive, gallery, gallery_detail, photo_date_detail, photo_day_archive, \
	photo_month_archive, photo_year_archive, photo, photo_detail, photolist

urlpatterns = patterns('',
	url(r'^$',
		 RedirectView.as_view(url=reverse_lazy('photologue:pl-gallery-archive')),
		 name='pl-photologue-root'),
	 url(r'^gallery/(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\w{1,2})/(?P<slug>[\-\d\w]+)/$',
			 gallery_date_detail,
			 name='gallery-detail'),
	 url(r'^gallery/(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\w{1,2})/$',
			 gallery_day_archive,
			 name='gallery-archive-day'),
	 url(r'^gallery/(?P<year>\d{4})/(?P<month>[0-9]{2})/$',
			 gallery_month_archive,
			 name='gallery-archive-month'),
	 url(r'^gallery/(?P<year>\d{4})/$',
			 gallery_year_archive,
			 name='pl-gallery-archive-year'),
	 url(r'^gallery/$',
			 gallery,
			 name='pl-gallery-archive'),
	 url(r'^gallery/(?P<slug>[\-\d\w]+)/$',
			gallery_detail,
			name='pl-gallery'),
	 url(r'^gallerylist/$',
			 gallerylist,
			 name='gallery-list'),

	 url(r'^photo/(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\w{1,2})/(?P<slug>[\-\d\w]+)/$',
			 photo_date_detail,
			 name='photo-detail'),
	 url(r'^photo/(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\w{1,2})/$',
			 photo_day_archive,
			 name='photo-archive-day'),
	 url(r'^photo/(?P<year>\d{4})/(?P<month>[0-9]{2})/$',
			 photo_month_archive,
			 name='photo-archive-month'),
	 url(r'^photo/(?P<year>\d{4})/$',
			 photo_year_archive,
			 name='pl-photo-archive-year'),
	 url(r'^photo/$',
			 photo,
			 name='pl-photo-archive'),

	 url(r'^photo/(?P<slug>[\-\d\w]+)/$',
			 photo_detail,
			 name='pl-photo'),
	 url(r'^photolist/$',
			 photolist,
			 name='photo-list'),

	 # Deprecated URLs.
	 url(r'^gallery/page/(?P<page>[0-9]+)/$',
			 GalleryListView.as_view(),
			 {'deprecated_pagination': True},
			 name='pl-gallery-list'),
	 url(r'^photo/page/(?P<page>[0-9]+)/$',
			 PhotoListView.as_view(),
			 {'deprecated_pagination': True},
			 name='pl-photo-list'),

	 url(r'^gallery/(?P<year>\d{4})/(?P<month>[a-z]{3})/(?P<day>\w{1,2})/(?P<slug>[\-\d\w]+)/$',
			 GalleryDateDetailOldView.as_view(),
			 name='pl-gallery-detail'),
	 url(r'^gallery/(?P<year>\d{4})/(?P<month>[a-z]{3})/(?P<day>\w{1,2})/$',
			 GalleryDayArchiveOldView.as_view(),
			 name='pl-gallery-archive-day'),
	 url(r'^gallery/(?P<year>\d{4})/(?P<month>[a-z]{3})/$',
			 GalleryMonthArchiveOldView.as_view(),
			 name='pl-gallery-archive-month'),
	 url(r'^photo/(?P<year>\d{4})/(?P<month>[a-z]{3})/(?P<day>\w{1,2})/(?P<slug>[\-\d\w]+)/$',
			 PhotoDateDetailOldView.as_view(),
			 name='pl-photo-detail'),
	 url(r'^photo/(?P<year>\d{4})/(?P<month>[a-z]{3})/(?P<day>\w{1,2})/$',
			 PhotoDayArchiveOldView.as_view(),
			 name='pl-photo-archive-day'),
	 url(r'^photo/(?P<year>\d{4})/(?P<month>[a-z]{3})/$',
			 PhotoMonthArchiveOldView.as_view(),
			 name='pl-photo-archive-month'),
 )

��    F      L  a   |         5     6   7  )   n     �  	   �     �     �  
   �  V   �     7     >     F     L     S     b     n     |     �     �     �     �     �     �               !     9     R     g          �     �  	   �     �     �  ?   �  	   	     #	     )	     <	     Y	     u	     �	     �	     �	  
   �	     �	     �	     �	     �	  	   �	  
   �	     
  
   
     
     (
  	   /
     9
     A
     G
     L
     R
     Y
     h
     n
     s
     x
  
   ~
  
   �
  k  �
  C      �   D  @   �  "     "   1  2   T     �     �  �   �     y     �     �     �  &   �     �      �  $     &   ?      f  "   �  !   �     �  (   �          "  )   3  )   ]  '   �     �      �     �                7  &   L  f   s     �     �  "     K   .  M   z  9   �  I     K   L     �     �     �     �     �     �     �       $   "     G     [  
   l     w     �     �     �     �     �  "   �     �     �               "     8            :   )             E              A                     6                     >                   9       (      1   ,   4          *   .   5   @         D          C   	       =          &   3             !      B   /   '      <          "   %                      #         7          ;   $      ?   -          F   8         0      
   2       +    
				    page %(page_number)s of %(total_pages)s
				 A "slug" is a unique URL-friendly title for an object. A gallery with that title already exists. Add a photo Add photo Adjacent gallery images: All galleries All photos All uploaded photos will be given a title made up of this title + a sequential number. Cancel Caption Close Create Delete gallery Description Filter by day Filter by month Filter by year Galleries for %(show_day)s Galleries for %(show_month)s Galleries for %(show_year)s Gallery Latest photo galleries Latest photos Next No galleries were found No galleries were found. No photos were found Photos for %(show_day)s Photos for %(show_month)s Photos for %(show_year)s Previous Published Remove Remove photo Select an existing gallery, or enter a title for a new gallery. Thumbnail Title View all galleries View all galleries for month View all galleries for year View all photos View all photos for month View all photos for year View gallery brightness caption color contrast count crop from date added date published date taken description effect galleries gallery image name photo photos rotate or flip sites slug tags title title slug view count Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-06-02 02:31+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
				    страница %(page_number)s от %(total_pages)s
				 "Slug" е уникално заглавие за обект, което е удобно за добавяне в хипервръзки. Галерия с това име вече съществува. Добавяне на снимка Добавяне на снимка Съседни снимки в галерията: Всички галерии Всички снимки На всички качени снимки ще бъде сложено заглавие, създадено от това заглавие + идентификационен номер. Прекъсване Надпис Затваряне Създаване Изтриване на галерия Описание Филтриране по ден Филтриране по месец Филтриране по година Галерии за %(show_day)s Галерии за %(show_month)s Галерии за %(show_year)s Галерия Последни фото галерии Последни снимки Следваща Не са намерени галерии Не са намерени галерии Не са намерени снимки Снимки за %(show_day)s Снимки за %(show_month)s Снимки за %(show_year)s Предишна Публикувано Премахване Премахване на снимка Изберете съществуваща галерия или въведете име за нова. мини-изображение Заглавие Към всички галерии Визуализиране на всички галерии за месец Визуализиране на всички галерии за година Визуализиране на всички снимки Визуализиране на всички снимки за месец Визуализиране на всички снимки за година Към галерия яркост надпис цвят контраст брой отрязване от дата на добавяне дата на публикуване взета дата описание ефект галерии галерия изображение име снимка снимки завърти или обърни сайтове slug тагове заглавие заглавен slug брой прегледи 